# execute the following command to build the simulation system
# note that changes to the below path and in config.yml are required to match the location of the modified version of GROMACS
python3 /work001/software/gromacs-bekker-2022/bekker-scripts/autoBuild.py config.yml
