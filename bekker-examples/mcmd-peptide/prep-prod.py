import os

name = "BIM99"
mode = ""

# module load 11.6.2; python prep-prod.py

name += "_"

Nbakapara = 30
lastPre_mdx = 17

gmxrunner = "/gs/hs1/hp190027/mcmd/software/gmxrunner.py"
gmx = "/gs/hs1/hp190027/mcmd/software/gromacs-2021-bekker/build/bin/gmx"


mdp = """integrator               = md
nsteps                   = 125000000 ; 250 ns
dt                       = 0.002
nstenergy                = 50000
nstlog                   = 50000
continuation             = yes
constraint_algorithm     = lincs
constraints              = h-bonds
lincs_iter               = 1
lincs_order              = 4
cutoff-scheme            = Verlet
ns_type                  = grid
nstlist                  = 40
rcoulomb                 = 1.2
rvdw                     = 1.2
coulombtype              = Reaction-Field
epsilon-rf               = 0
tcoupl                   = V-rescale
tc-grps                  = Protein Water_and_ions
tau_t                    = 0.0   0.0
ref_t                    = 300.0 300.0
pcoupl                   = no
pbc                      = xyz
DispCorr                 = EnerPres
fb-histogram-file        = /gs/hs1/hp190027/mcmd/BIM-folding/ff99SB-ILDN/mcmd/histograms/mdx-%s.dat
gen-vel                  = no
do-fb-mcmd               = yes
fb-include-restrain-epot = yes
fb-base-temp             = 300.0

nstxout-compressed       = 2500 ; save the entire structure every 5ps
compressed-x-grps        = System
"""%(lastPre_mdx,)

yml = """mode: tpr
pbs-loc: job.pbs

job-name: "%s"

email-log: [gertjan.bekker@protein.osaka-u.ac.jp]

qsub: "qsub -g hp190021 -l q_node=1 -l h_rt=24:00:00 job.pbs"

jobs:
  - "$GMX mdrun -deffnm run -ntmpi 1 -ntomp 7 -cpi -noappend -maxh 24 -notunepme"

# set this to false to prevent the script from running in the next iteration
active: true
"""

pbs = """#!/bin/bash
#$ -S /bin/bash
#$ -cwd

. /etc/profile.d/modules.sh
module load cuda/11.2.146
module load intel-python/3.6.5

PATH=/apps/t3/sles12sp2/uge/latest/bin/lx-amd64:$PATH

# set the location of the runner script & gromacs
export RUNNER=%s
export GMX=%s

cd ${SGE_O_WORKDIR}

# simly run the runner script; it'll take care of everything...
python $RUNNER config.yml
"""%(gmxrunner, gmx)

CWD = os.getcwd()

if mode == "prepare":
  os.mkdir("prod")
  os.chdir("prod")
  
  for i in range(Nbakapara):
    if not os.path.exists(str(i)): os.mkdir(str(i))
    os.chdir(str(i))
    
    open("config.yml", "w").write(yml%(name+str(i)))
    open("job.pbs", "w").write(pbs)
    open("run.mdp", "w").write(mdp)
    
    try: os.remove("run.tpr")
    except: pass
    
    os.system("%s grompp -f run.mdp -c ../../mdx-%s/%s.gro -t ../../mdx-%s/%s.cpt -p ../../../build/topol.top -o run.tpr -n ../../../build/index.ndx"%(gmx, lastPre_mdx, i, lastPre_mdx, i))
    
    os.chdir("..")

elif mode == "submit":
  os.chdir("prod")
  
  for i in range(Nbakapara):
    os.chdir(str(i))
    
    os.system("python %s config.yml --submit"%gmxrunner)
    
    os.chdir("..")

elif mode == "update":
  os.chdir("prod")
  
  for i in range(Nbakapara):
    os.chdir(str(i))
    
    open("config.yml", "w").write(yml%(name+str(i)))
    
    os.chdir("..")
