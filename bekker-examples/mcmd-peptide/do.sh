# execute the following command to start the McMD simulations 
# this will automatically execute all pre-run iterations
# note that changes to the below path and in config.yml are required to match the location of the modified version of GROMACS
# note 2; additional changes also need to be made to config.yml, depending on the computational system where this is executed
python3 /work001/software/gromacs-bekker-2022/bekker-scripts/fbmd.py config.yml
