# pip install --user PyYAML
import sys, os, yaml, subprocess, shutil, smtplib, random, time, glob, math, numpy as np, signal, shlex

from email import *
from email.mime.multipart import *
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate, formataddr
from email import encoders as Encoders
from email.header import Header
from email.mime.text import MIMEText

conf_file = sys.argv[1]

# load config & init
CWD = os.path.abspath(os.path.dirname(os.getcwd()+os.sep+conf_file))+os.sep
conf_file = os.path.basename(conf_file)
os.chdir(CWD)

job_start_time = time.time()

def autoAddParameters(parameters):
  tmp = config["qsub"].split(" ")
  for k,v in parameters:
    if not k in tmp: tmp += [k, v]
  config["qsub"] = " ".join(tmp)

config = yaml.safe_load(open(CWD+conf_file).read())

if not config.get("active"): exit()

if not os.path.exists("logs"): os.mkdir("logs")

mps = None
mpsworkdir = None
if config.get("mps") and not "--submit" in sys.argv:
  if not os.path.exists("mps"): os.mkdir("mps")
  if os.environ.get("CUDA_MPS_PIPE_DIRECTORY", "/tmp/nvidia-mps") == "/tmp/nvidia-mps": os.environ["CUDA_MPS_PIPE_DIRECTORY"] = mpsworkdir = os.getcwd()+"/mps/"
  try:
    with open(os.getcwd()+"/logs/mps.log", "w") as fp: mps = subprocess.Popen(["nvidia-cuda-mps-control", "-f"], cwd=os.getcwd(), env=os.environ.copy(), stdout=fp, stderr=fp)
  except: pass

scheduler = config.get("scheduler")
if scheduler is None:
  if config["qsub"].startswith("sbatch"): scheduler = "slurm"
  elif config["qsub"].startswith("pjsub"): scheduler = "fugaku"
  elif config["qsub"].startswith("qsub"): scheduler = "tsubame"

email_address = None

if scheduler == "tsubame": 
  autoAddParameters([("-N", config["job-name"]), ("-o", "'logs/'"), ("-e", "'logs/'")])  
  email_address = "pbsusr@g.gsic.titech.ac.jp"
  
if scheduler == "fugaku": 
  email_address = None#"pbsusr@riken.jp" # not supported of fukaku

VCP_settings = {
  "jobs-per-gpu": 1,
  "cores": 8
}

if "VCP" in config:
  VCP_settings["jobs-per-gpu"] = config["VCP"].get("jobs-per-gpu", VCP_settings["jobs-per-gpu"])
  VCP_settings["cores"] = config["VCP"].get("cores", VCP_settings["cores"])

jobid = None
for i in ["SLURM_JOB_ID"]:
  jobid = os.environ.get(i)
  if jobid is not None: break
if jobid is None: jobid = os.getpid()

email_address = config.get("from-email", email_address)

def __sendMail__(From="", To=[], CC=[], BCC=[], Subject="", Message="", Files=[]):
  if type(To) != list: To = [To]
  if type(CC) != list: To = [CC]
  if type(BCC) != list: To = [BCC]
  if type(Files) != list: Files = [Files]
  if len(To) == 0: return
  msg = MIMEMultipart()
  if type(From) == list: msg["From"] = formataddr((str(Header(From[0], "utf-8")), From[1]))
  else: msg["From"] = From
  msg["To"] = COMMASPACE.join(To)
  msg["Cc"] = COMMASPACE.join(CC)
  msg["Bcc"] = COMMASPACE.join(BCC)
  msg["Date"] = formatdate(localtime=True)
  msg["Subject"] = Subject
  msgMU = MIMEMultipart("alternative")
  msg.attach(msgMU)
  if type(Message) == list:
    for m in Message: msgMU.attach(MIMEText(m[0].encode("utf-8"), m[1], "UTF-8"))
  else: msgMU.attach(MIMEText(Message.encode("utf-8"), "html", "UTF-8"))
  for f in Files:
    part = MIMEBase("application", "octet-stream")
    if type(f) == str or type(f) == bytes:
      part.set_payload(open(f, "rb").read())
      Encoders.encode_base64(part)
      part.add_header("Content-Disposition", "attachment; filename=\"%s\""%(os.path.basename(f)))
    elif type(f) == list:
      part.set_payload(f[1])
      Encoders.encode_base64(part)
      part.add_header("Content-Disposition", "attachment; filename=\"%s\""%(os.path.basename(f[0])))
    msg.attach(part)
  smtp = smtplib.SMTP("localhost")
  smtp.sendmail(type(From) == list and From[1] or From, To+CC+BCC, msg.as_string())
  smtp.close()

# Virtual Cluster Partition
class VCP:
  def __init__(self, threads=[], GPUid=None):
    self.threads = threads
    self.GPUid = GPUid
    
  def execute(self, cwd, cmd, stdout, stderr):
    env = os.environ.copy()
    env["OMP_NUM_THREADS"] = str(len(self.threads))
    if self.GPUid is not None: env["CUDA_VISIBLE_DEVICES"] = str(self.GPUid)
    threads = ",".join([str(i) for i in self.threads])
    
    stdout.write(f"VCP config: \nthreads: {threads}; GPUs: {self.GPUid}\n\n") # for now (debugging)
    
    #p = subprocess.Popen(shlex.split(f"numactl --physcpubind={threads} "+cmd), stdout=stdout, stderr=stderr, cwd=cwd, env=env)
    p = subprocess.Popen(f"numactl --physcpubind={threads} "+cmd, stdout=stdout, stderr=stderr, cwd=cwd, env=env, shell=True)
    return p.communicate()

def getGPUlist():
  p = subprocess.Popen(shlex.split("/usr/bin/nvidia-smi topo -m"), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  stuff = p.communicate()[0].decode("utf-8").splitlines()

  idx = None
  GPUs = []
  for line in stuff:
    if not (line.startswith("GPU") or idx is None): continue
    line = line.split("\t")
    if idx is None: idx = line.index("CPU Affinity")
    else:
      GPUid = int(line[0][3:])
      affinity = []
      for part in line[idx].split(","):
        a,b = part.split("-")
        a = int(a)
        b = int(b)+1
        affinity += list(range(a,b))
      GPUs.append([GPUid, affinity])
  return dict(GPUs)

def getCPUlist():
  p = subprocess.Popen(shlex.split("/usr/bin/numactl --show"), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  info = dict([[j.strip() for j in i.decode("utf-8").split(":")] for i in p.communicate()[0].splitlines()])
  return [int(i) for i in info["physcpubind"].split()]

def getPhysicalCoreLayout():
  threads = {}
  TID = None
  for line in open("/proc/cpuinfo"):
    key, value = [i.strip() for i in line.partition(":")[::2]]
    if key == "processor":
      TID = int(value)
      threads[TID] = {}
    threads[TID][key] = value
  
  layout = {}
  
  for thread, info in threads.items():
    physical_id = int(info["physical id"])
    core_id = (physical_id*int(info["cpu cores"]))+int(info["core id"])
    if not physical_id in layout: layout[physical_id] = {}
    if not core_id in layout[physical_id]: layout[physical_id][core_id] = []
    layout[physical_id][core_id].append(thread)
  return layout
  
def chunkList(l, n):
  for i in range(0, len(l), n): yield l[i:i+n]

  
def constructVCPs():
  layout = getPhysicalCoreLayout()
  avail_threads = getCPUlist()
  
  real_cores = []
  c2t = {}
  
  for numa_id, numa in layout.items():
    for core, threads in numa.items(): 
      real_cores.append(threads[0])
      c2t[threads[0]] = threads
  avail_cores = np.intersect1d(avail_threads, real_cores)
  
  GPUinfo = getGPUlist()
  GPUctr = dict([[gpuid, 0] for gpuid, gputhreads in GPUinfo.items()])
  maxjobspergpu = VCP_settings["jobs-per-gpu"]
  cores_per_vcp = VCP_settings["cores"]

  VCPs = []
  for cores in chunkList(avail_cores, cores_per_vcp):
    if len(cores) != cores_per_vcp: break
    threads = np.sort(np.concatenate([c2t[c] for c in cores])).tolist()
    GPUID = None
    for gpuid, gputhreads in GPUinfo.items():
      if threads[0] in gputhreads and GPUctr[gpuid] < maxjobspergpu:
        GPUID = gpuid
        GPUctr[gpuid] += 1
        break
    VCPs.append(VCP(threads, GPUID))
  
  return VCPs
 
def checkProgress(deffnm, cwd=os.getcwd()):
  cwd += os.sep
  items = []
  for fl in os.listdir(cwd):
    if fl[-4:] != ".log" or fl[:len(deffnm)+5] != deffnm+".part": continue
    items.append(int(fl.split(".")[-2][4:]))
  
  nsteps = 0
  nStart = 0
  nEnd = 0
  
  PID = str(max(items)).rjust(4, "0")
  data = open(cwd+"%s.part%s.log"%(deffnm, PID)).readlines()
  timeMode = False
  for line in data:
    line = line.strip()
    if not line: continue
    if timeMode and line[:5] == "Using": timeMode = False

    if line[:6] == "nsteps": nsteps = int(line.split("=")[1].strip())
    
    if timeMode:
      tmp = line.split(":")
      if tmp[0] == "step": nStart = int(tmp[1].strip())
    
    if line == "Reading checkpoint file %s.cpt"%deffnm: timeMode = True
    
    if line[:24] == "Writing checkpoint, step": nEnd = int(line[25:].split()[0])
  
  if "overwrite-nsteps" in config: nsteps = config["overwrite-nsteps"]
  
  return PID, nsteps, nStart, nEnd
  
def getDIR(deffnm, jidx):
  directories = config.get("directories")
  if type(directories) == dict and deffnm in directories: return directories[deffnm]
  elif type(directories) == list and jidx < len(directories): return directories[jidx]
  
def handle_tpr_job(cmd, stdout, stderr, cwd, VCP):
  global job_error
  
  tmp = cmd.split()
  deffnm = tmp[tmp.index("-deffnm")+1]
  if os.path.exists(deffnm+".cpt"):
    PID, nsteps, nStart, nEnd = checkProgress(deffnm, cwd)
    if nEnd == nsteps: return # should never be triggered...
  else: PID = "0000"
  
  cmd = setWT(cmd)
  if not "-cpi" in tmp: cmd += " -cpi"
  if not "-noappend" in tmp: cmd += " -noappend"

  VCP.execute(cwd, cmd, stdout, stderr)
  
  PID2, nsteps2, nStart2, nEnd2 = checkProgress(deffnm, cwd)
  if nEnd2-nEnd < 10:
    job_error = True
    return False
    
  shutil.copy(cwd+"/%s.cpt"%deffnm, cwd+"/%s.part%s.cpt"%(deffnm, PID2))
  
  return nEnd2 < nsteps2

job_needs_continue = False
job_error = False

def processJob(job):
  global job_needs_continue
  
  cwd = job["cwd"]
  VCP = job["VCP"]
  
  with open(job["logbase"]+".stdout", "w") as stdout, open(job["logbase"]+".stderr", "w") as stderr:
    for step in job["steps"]:
      if len(glob.glob(cwd+"/"+step["file"])): continue # required file exists..
      cmd = step["cmd"]
      if step.get("tpr") == True:
        if handle_tpr_job(cmd, stderr, stdout, cwd, VCP):
          job_needs_continue = True
          return
      else: 
        cmd = setWT(cmd)
        VCP.execute(cwd, cmd, stdout, stderr)
      if not len(glob.glob(cwd+"/"+step["file"])): 
        job_error = True
        return -1 # failure

def setWT(cmd):
  tmp = cmd.split()
  WT = float(config.get("wall-time-hours", 24))
  if not "-maxh" in tmp and WT and tmp[1] == "mdrun":
    hrs_running = (time.time()-job_start_time)/3600
    remaining = math.floor(((WT-hrs_running)/.99)*100)/100
    remaining -= .025 # give gmx 90 seconds to quit
    cmd += f" -maxh {remaining}"
  return cmd

import threading

def multiCMDMode():
  "the goal of this version is to execute multiple commands, and that the system is smart enough to understand at which stage it is so that it can auto-resume..."
  
  CWD = os.getcwd()
  
  msg = ""

  processes = []
  sanity = {}
  
  threads = []
  
  VCPs = constructVCPs()
  
  unique_cwd = set()
  for job in config["jobs"]: unique_cwd.add(job["cwd"])
  
  for cwd in unique_cwd:
    if not os.path.exists(cwd+"/logs/"): os.mkdir(cwd+"/logs/")
  
  if len(unique_cwd) == len(config["jobs"]):
    for job in config["jobs"]: job["logbase"] = job["cwd"]+f"/logs/{jobid}"
  else:
    for idx, job in enumerate(config["jobs"]): job["logbase"] = job["cwd"]+f"/logs/{jobid}_{idx}"
  
  for job in config["jobs"]:
    job["VCP"] = VCPs.pop(0)
    thread = threading.Thread(target=processJob, args=(job,))
    thread.start()
    threads.append(thread)
  
  for thread in threads: thread.join()

  if job_needs_continue:
    if scheduler == "fugaku":
      hosts = [f"login{i}" for i in range(1, 7)]
      random.shuffle(hosts)
      for i in range(20):
        proc = subprocess.Popen(["ssh", hosts[i%len(hosts)], "cd %s; %s"%(os.getcwd(), config["qsub"])], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        if proc.returncode == 0: break
        print(stdout, stderr)
    else: os.system(config["qsub"])
  elif email_address is not None: 
    if job_error: __sendMail__(email_address, config.get("email-log", []), [], [], "Job "+config["job-name"]+" failed.", "", [])
    else: __sendMail__(email_address, config.get("email-log", []), [], [], "Job "+config["job-name"]+" finished.", "", [])
  if not job_needs_continue:
    with open("done", "w") as fp: fp.write("")

def dummyCheckJobs():
  if config.get("mode") != "multi-cmds":
    print("ERROR: mode is not set to multi-cmds")
    exit()
    
  for job in config["jobs"]:
    for step in job["steps"]:
      if step.get("tpr") == True and not "*" in step["file"]:
        print("ERROR: wildcard is missing in file for step job")
        exit()

if "--submit" in sys.argv:
  print(config["qsub"])
  dummyCheckJobs()
  os.system(config["qsub"])
  exit()
  
if config.get("mode") == "tpr": exit()
if config.get("mode") == "multi-cmds": multiCMDMode()
if mps is not None: 
  mps.kill()
  if mpsworkdir is not None: os.system(f"rm -rf "+mpsworkdir)
