import os, mdtraj, yieldxtc, numpy as np, scipy, scipy.spatial, scipy.misc, scipy.ndimage, json, itertools, struct, sys
import sklearn, sklearn.decomposition

from adjustText import adjust_text

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

mpl.rcParams['xtick.major.size'] = 7
mpl.rcParams['ytick.major.size'] = 7
mpl.rcParams['xtick.major.width'] = 3
mpl.rcParams['ytick.major.width'] = 3
mpl.rcParams['axes.axisbelow'] = True
mpl.rc('font', **{'size': 16})

cwd = os.getcwd()+os.sep
import tools

def additionalArguments(optparser):
  optparser.add_argument("--stage1", action="store_true")
  optparser.add_argument("--stage2", action="store_true")
  optparser.add_argument("--stage3", action="store_true")
  
  optparser.add_argument("--prep-only", action="store_true", required=False)
  optparser.add_argument("--temp", type=float, default=300, required=False)
  optparser.add_argument("--grid", action="store_true", required=False)
  optparser.add_argument("--no-exp", action="store_true", required=False)

tools.setup(additionalArguments)

mdx = tools.RESTART["production-phase"]
Nbakapara = tools.config["fbmd"]["seeds"]

refGro = tools.analFolder+"ref.gro"

maxPMF = tools.config["analysis"]["pca"]["max-pmf"]
nBins = tools.config["analysis"]["pca"].get("plot-nbins", 50)
upscaleN = tools.config["analysis"]["pca"].get("plot-upscale", 50)

ref = mdtraj.load(refGro)
outputIndexes = tools.outputIndexes()
indicesSI = outputIndexes.receptorCalpha

foldingMode = not "ligand" in tools.config
if not foldingMode: pcaIdxs = outputIndexes.ligandAtoms
else: pcaIdxs = outputIndexes.receptorAtoms 

if tools.config["analysis"].get("si-posres"):
  prfile = tools.config["fbmd"].get("restraints", {}).get("posres-file")
  if not prfile: 
    print("No position restraints used, while si-posres was set...")
    exit()
  indicesSI = []
  for line in open(tools.CWD+prfile).readlines()[1:]:
    if not line.strip(): continue
    line = line.split()
    indicesSI.append(np.where(int(line[0]) == outputIndexes.raw)[0][0])
  indicesSI = np.array(indicesSI)
  print(len(indicesSI), "atoms selected for superposition")

def load_data(): 
  for seed in range(Nbakapara):
    traj = tools.load(tools.analFolder+"cleaned/%s.xtc"%seed, top=ref.top)
    traj.superpose(ref, 0, indicesSI)
    traj.atom_slice(pcaIdxs, inplace=True)
    yield seed, traj.xyz.reshape((traj.xyz.shape[0], traj.xyz.shape[1]*3)), np.fromfile(tools.analFolder+"cleaned/%s.fb.ene"%seed, dtype=np.float32)

def stage1():
  pca = sklearn.decomposition.IncrementalPCA(n_components=min(len(pcaIdxs)*3, 100))
  
  tempRange = tools.config["analysis"]["pca"].get("pca-temp-range", None)
  
  jso = json.loads(open(tools.analFolder+"distributions/reweighting.json").read())
  tempss = [i for i in jso.keys() if not i in ["energy-bins", "internal"]]
  temps = np.array([float(i) for i in tempss])
  
  if tempRange is not None:
    binone = tempss[np.argmin(np.abs(tempRange[0]-temps))]
    bintwo = tempss[np.argmin(np.abs(tempRange[1]-temps))]
    ok = np.where((np.exp(jso[binone]) > 0) | (np.exp(jso[bintwo]) > 0))[0]
    baho = np.array(jso["energy-bins"])
    minE = baho[np.min(ok)]
    maxE = baho[np.max(ok)+1]
  
  buffer = []

  for seed, xyzs, ene in load_data():
    if tempRange is not None:
      ok = np.where((ene >= minE) & (ene < maxE))[0]
      if len(ok) == 0: continue
      xyzs = xyzs[ok]
    if len(buffer): buffer = np.concatenate([buffer, xyzs])
    else: buffer = xyzs
    if len(buffer) < 1000: continue
    xyzs = buffer
    buffer = []
    pca.partial_fit(xyzs)

  jso = {}
  jso["components_"] = pca.components_.tolist()
  jso["explained_variance_"] = pca.explained_variance_.tolist()
  jso["explained_variance_ratio_"] = pca.explained_variance_ratio_.tolist()
  jso["singular_values_"] = pca.singular_values_.tolist()
  jso["mean_"] = pca.mean_.tolist()
  jso["n_components_"] = pca.n_components_
  try: jso["noise_variance_"] = pca.noise_variance_.tolist()
  except: jso["noise_variance_"] = [pca.noise_variance_]
  open(tools.analFolder+"temp/pca_xyz_sklearn.json", "w").write(json.dumps(jso))

def stage2():
  T = round(tools.cli_args.temp)
  RT = 0.008314462175 * T / 4.184

  bins, pdf_log = tools.getReweigingProb(T)
  pdf_min = bins[0]
  nob = len(pdf_log)-1
  pdf_binSize = bins[1]-bins[0]

  if "gomi-native" in tools.config["analysis"]:
    gomi = tools.load(tools.analFolder+tools.config["analysis"]["gomi-native"])
    print('gomi')

    gomi.superpose(ref, 0, indicesSI)
    gomi.atom_slice(pcaIdxs, inplace=True)
    nativeXYZ = gomi.xyz.reshape((gomi.xyz.shape[0], gomi.xyz.shape[1]*3))[0]
  else:
    tmp = ref.atom_slice(pcaIdxs, inplace=False)
    nativeXYZ = tmp.xyz.reshape((tmp.xyz.shape[0], tmp.xyz.shape[1]*3))[0]
    
  pca = sklearn.decomposition.IncrementalPCA(n_components=100)
  jso = json.load(open(tools.analFolder+"temp/pca_xyz_sklearn.json"))
  pca.components_ = np.array(jso["components_"])
  pca.explained_variance_ = np.array(jso["explained_variance_"])
  pca.explained_variance_ratio_ = np.array(jso["explained_variance_ratio_"])
  pca.singular_values_ = np.array(jso["singular_values_"])
  pca.mean_ = np.array(jso["mean_"])
  pca.n_components_ = np.array(jso["n_components_"])
  pca.noise_variance_ = np.array(jso["noise_variance_"])

  pc1 = []
  pc2 = []
  lnP = []

  for seed, xyzs, ene in load_data():
    bins = np.floor((ene-pdf_min)/pdf_binSize).astype(int)
    bins[bins < 0] = 0
    bins[bins > nob] = nob

    pcs = pca.transform(xyzs).T
    pc1.append(pcs[0])
    pc2.append(pcs[1])
    lnP.append(pdf_log[bins])

  pc1 = np.concatenate(pc1)
  pc2 = np.concatenate(pc2)
  lnP = np.concatenate(lnP)

  nativePC = pca.transform([nativeXYZ])[0][:2]
  print("nativePC", nativePC)

  x1 = np.min(pc1)
  x2 = np.max(pc1)
  y1 = np.min(pc2)
  y2 = np.max(pc2)

  x1 -= (x2-x1)*0.025
  x2 += (x2-x1)*0.025

  y1 -= (y2-y1)*0.025
  y2 += (y2-y1)*0.025

  mn1 = x1
  bs1 = (x2-x1)/nBins

  mn2 = y1
  bs2 = (y2-y1)/nBins

  idx1 = np.floor((pc1-mn1)/bs1).astype(int)
  idx2 = np.floor((pc2-mn2)/bs2).astype(int)

  preGrid = [[[] for j in range(nBins)] for i in range(nBins)]

  for i in range(len(lnP)): preGrid[idx1[i]][idx2[i]].append(lnP[i])

  grid = np.array([[0.0 for j in range(nBins)] for i in range(nBins)])

  for i in range(nBins):
    for j in range(nBins):
      if len(preGrid[i][j]): grid[i][j] = np.logaddexp.reduce(np.sort(preGrid[i][j]))
      else: grid[i][j] = -np.inf

  PMF = -RT * (grid - np.logaddexp.reduce(np.sort(lnP)))

  PMF_lower = np.min(PMF)
  PMF_upper = np.max(PMF)-PMF_lower


  PMF -= PMF_lower

  if T == 300: open(tools.analFolder+"temp/pca_xyz_fel2.json", "w").write(json.dumps({"PMF": PMF.tolist(), "mn1": mn1, "bs1": bs1, "mn2": mn2, "bs2": bs2, "PMF_upper": PMF_upper}))
  else: open(tools.analFolder+"temp/pca_xyz_fel2_%s.json"%(T,), "w").write(json.dumps({"PMF": PMF.tolist(), "mn1": mn1, "bs1": bs1, "mn2": mn2, "bs2": bs2, "PMF_upper": PMF_upper}))

  if tools.cli_args.prep_only: exit()

  nBinsX, nBinsY = np.shape(PMF)

  x1 = mn1 - bs1
  x2 = x1 + ((nBinsX+2)*bs1)
  y1 = mn2 - bs2
  y2 = y1 + ((nBinsY+2)*bs2)

  X, Y = np.meshgrid(np.array([(i*bs1)+mn1 for i in range(nBinsX)]), np.array([(j*bs2)+mn2 for j in range(nBinsY)]))
  new_X = scipy.ndimage.zoom(X, upscaleN, order=1)
  new_Y = scipy.ndimage.zoom(Y, upscaleN, order=1)

  wasjunk = PMF == np.inf
  wasjunk_new = scipy.ndimage.zoom(wasjunk.astype(np.float32), upscaleN, order=1)
  wasjunk_new = scipy.ndimage.gaussian_filter(wasjunk_new, upscaleN*.25)

  PMF[wasjunk] = np.max(PMF[~wasjunk])+1
  new_PMF = scipy.ndimage.zoom(PMF, upscaleN, order=1)
  new_PMF = scipy.ndimage.gaussian_filter(new_PMF, upscaleN*.25)
  new_PMF -= np.min(new_PMF)
  new_PMF[new_PMF >= np.mean(new_PMF[wasjunk_new >= 0.25])] = np.inf

  plt.xlabel("PC1")
  plt.ylabel("PC2")

  ppp = np.ma.masked_invalid(new_PMF.T)
  #CS = plt.contour(new_X, new_Y, ppp, linewidths=0.5, colors='k', levels=[1,2,4])
  #plt.clabel(CS, fontsize=9, inline=1)
  CS = plt.pcolormesh(new_X, new_Y, ppp, cmap="jet", shading='gouraud')
  cbar = plt.colorbar(CS)
  cbar.set_label("Free energy (kcal/mol)")
  plt.axis((x1,x2,y1,y2))

  plt.scatter([nativePC[0]], [nativePC[1]], c='k', marker='x', s=256*1.0625, linewidth=3.0*1.25)
  plt.scatter([nativePC[0]], [nativePC[1]], c='w', marker='x', s=256, linewidth=3.0)
  
  plt.minorticks_on()
  if tools.cli_args.grid:
    plt.grid(which="major", linestyle='solid')
    plt.grid(which="minor", linestyle='dotted')

  plt.savefig(tools.analFolder+"fel_xyz_%sK.png"%(T,), dpi=600, bbox_inches='tight')
  plt.close()

  plt.xlabel("PC1")
  plt.ylabel("PC2")


  PMF[PMF > maxPMF+1] = maxPMF+1
  new_PMF = scipy.ndimage.zoom(PMF, upscaleN, order=1)
  new_PMF = scipy.ndimage.gaussian_filter(new_PMF, upscaleN*.25)
  new_PMF -= np.min(new_PMF)

  badbad = new_PMF > maxPMF
  new_PMF[badbad] = np.nan

  ppp = np.ma.masked_invalid(new_PMF.T)
  #CS = plt.contour(new_X, new_Y, ppp, linewidths=0.5, colors='k', levels=[1,2,4])
  #plt.clabel(CS, fontsize=9, inline=1)
  CS = plt.pcolormesh(new_X, new_Y, ppp, cmap="jet", shading='gouraud')
  plt.clim(0, maxPMF)
  cbar = plt.colorbar(CS)
  cbar.set_label("Free energy (kcal/mol)")
  plt.axis((x1,x2,y1,y2))

  plt.scatter([nativePC[0]], [nativePC[1]], c='k', marker='x', s=256*1.0625, linewidth=3.0*1.25)
  plt.scatter([nativePC[0]], [nativePC[1]], c='w', marker='x', s=256, linewidth=3.0)

  plt.minorticks_on()
  if tools.cli_args.grid:
    plt.grid(which="major", linestyle='solid')
    plt.grid(which="minor", linestyle='dotted')

  if maxPMF == tools.config["analysis"]["pca"]["max-pmf"]: plt.savefig(tools.analFolder+"fel_xyz_%sK_lim.png"%(T,), dpi=600, bbox_inches='tight')
  else: plt.savefig(tools.analFolder+"fel_xyz_%sK_lim_%s.png"%(T,maxPMF,), dpi=600, bbox_inches='tight')

  mean_x = np.mean(new_X[~badbad.T])
  std_x = np.std(new_X[~badbad.T])
  mean_y = np.mean(new_Y[~badbad.T])
  std_y = np.std(new_Y[~badbad.T])
  x1 = mean_x - std_x*3
  x2 = mean_x + std_x*3
  y1 = mean_y - std_y*3
  y2 = mean_y + std_y*3

  pc1range = tools.config["analysis"]["pca"].get("pc1-range", [x1, x2])
  pc2range = tools.config["analysis"]["pca"].get("pc2-range", [y1, y2])
  plt.axis((pc1range[0], pc1range[1], pc2range[0], pc2range[1]))

  plt.minorticks_on()
  if tools.cli_args.grid:
    plt.grid(which="major", linestyle='solid')
    plt.grid(which="minor", linestyle='dotted')
    
  if not tools.cli_args.no_exp:
    txt = plt.text(nativePC[0], nativePC[1], "Exp", fontdict={"color": "black", "size": 12})
    txt.set_bbox(dict(boxstyle="circle,pad=0.1", facecolor='white', alpha=0.75, edgecolor='none'))
    adjust_text([txt], expand_text=(1.2, 1.4), expand_points=(1.4, 1.8), force_text=(1.0, 1.0), force_points=(1.0, 1.0), arrowprops=[dict(shrinkA=0, shrinkB=1.75, arrowstyle='-', color='black')], add_objects=[plt.text(nativePC[0], nativePC[1], "X", alpha=0, fontdict={"color": "black", "size": 12})])

  if maxPMF == tools.config["analysis"]["pca"]["max-pmf"]: plt.savefig(tools.analFolder+"fel_xyz_%sK_lim_zoom.png"%(T,), dpi=600, bbox_inches='tight')
  else: plt.savefig(tools.analFolder+"fel_xyz_%sK_lim_zoom_%s.png"%(T,maxPMF,), dpi=600, bbox_inches='tight')
  plt.close()


if tools.cli_args.stage1: stage1()
if tools.cli_args.stage2: stage2()

