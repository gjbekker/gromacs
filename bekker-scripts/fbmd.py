# Contact gertjan.bekker@protein.osaka-u.ac.jp for more info

# this script executes the fb-mcmd simulation on various clusters in an automated manner based on the input config.yml configuration file

# imports
import yaml, os, numpy as np, subprocess, time, sys, struct, datetime, random, shlex

from collections import OrderedDict
  
# make matplotlib optional...
import matplotlib as mpl
mpl.use('Agg')

import matplotlib.pyplot as plt
  
CAL = 4.184
  
# in the future this should be read in from the command line...
conf_file = sys.argv[1]

# load config & init
__initial_dir__ = os.getcwd()
CWD = os.path.abspath(os.path.dirname(os.getcwd()+os.sep+conf_file))+os.sep
hostloc = CWD

conf_file = os.path.basename(conf_file)
os.chdir(CWD)

config = None
def loadConfig():
  global config
  try: config = yaml.load(open(CWD+conf_file).read(), Loader=yaml.FullLoader)
  except: config = yaml.load(open(CWD+conf_file).read())

loadConfig()
__base__ = config["base"]
__compute__ = config["compute"]
GMX = __compute__["gmx"]
bekkerScripts = "/".join(GMX.split("/")[:-3])+"/bekker-scripts/"

def checkFile(tp, loc):
  if loc is not None and not os.path.exists(loc):
    print("config.yml setting", tp, "File does not exist:", loc)
    exit()

checkFile("base/pdb", __base__["pdb"])
checkFile("base/cpt", __base__.get("cpt"))
checkFile("base/tpl", __base__["tpl"])
checkFile("base/base_inp", __base__["base_inp"])
checkFile("base/index", __base__["index"])
checkFile("compute/gmx", GMX)
checkFile("compute/pbs-base", __compute__.get("pbs-base"))
checkFile("fbmd/restraints/posres-file", config["fbmd"].get("restraints", {}).get("posres-file"))
checkFile("fbmd/restraints/disres-file", config["fbmd"].get("restraints", {}).get("disres-file"))
checkFile("fbmd/restraints/comres-file", config["fbmd"].get("restraints", {}).get("comres-file"))
checkFile("fbmd/restraints/path-file", config["fbmd"].get("restraints", {}).get("path-file"))

restart_file = CWD + os.path.splitext(conf_file)[0]+".restart.yml"
try: 
  try: RESTART = yaml.load(open(restart_file).read(), Loader=yaml.FullLoader)
  except: RESTART = yaml.load(open(restart_file).read())
  if RESTART == None: RESTART = {}
except: RESTART = {}

PBSscript = None
if "pbs-base" in __compute__:
  with open(__compute__["pbs-base"]) as fp: PBSscript = fp.read()

class Logger(object):
  def __init__(self, fname, mode):
    self.terminal = sys.stdout
    self.log = open(fname, mode)

  def write(self, message):
    self.terminal.write(message)
    self.log.write(message)
    self.terminal.flush()
    self.log.flush()
    
class fauxLogger(object):
  def __init__(self, fname=None, mode=None):
    self.terminal = sys.stdout
  
  def write(self, message): 
    self.terminal.write(message)
    self.terminal.flush()

### processify (https://gist.github.com/schlamar/2311116)

import traceback
from functools import wraps
from multiprocessing import Process, Queue


def processify(func):
    '''Decorator to run a function as a process.
    Be sure that every argument and the return value
    is *pickable*.
    The created process is joined, so the code does not
    run in parallel.
    '''

    def process_func(q, *args, **kwargs):
        try:
            ret = func(*args, **kwargs)
        except Exception:
            ex_type, ex_value, tb = sys.exc_info()
            error = ex_type, ex_value, ''.join(traceback.format_tb(tb))
            ret = None
        else:
            error = None

        q.put((ret, error))

    # register original function with different name
    # in sys.modules so it is pickable
    process_func.__name__ = func.__name__ + 'processify_func'
    setattr(sys.modules[__name__], process_func.__name__, process_func)

    @wraps(func)
    def wrapper(*args, **kwargs):
        q = Queue()
        p = Process(target=process_func, args=[q] + list(args), kwargs=kwargs)
        p.start()
        ret, error = q.get()
        p.join()

        if error:
            ex_type, ex_value, tb_str = error
            message = '%s (in subprocess)\n%s' % (ex_value, tb_str)
            raise ex_type(message)

        return ret
    return wrapper

### END processify

def restart_program():
  sys.exit(3) # since nothing works, simply exit and have a parent runner take care of everything...
    
def saveRESTART(): open(restart_file, "w").write(yaml.dump(RESTART))

def loadMD(inp):
  obj = OrderedDict()
  for line in inp.splitlines():
    line = line.split(";")[0].strip()
    if not line or line[0] == ";": continue
    line = line.partition("=")
    obj[line[0].strip()] = line[2].strip()
  return obj
    
def saveMD(inp):
  out = ""
  n = max([len(i) for i in inp.keys()])
  for k,v in inp.items(): out += ("%-"+str(n)+"s = %s\n")%(k, v)
  return out
 
def generate_offload(self, grompp_options, mdrun_options, PBS_settings, mdx, cwd):
  for seed, mdp_file, struc_file, restart_file in self.simulations:
    seedrun = seed
    if mdp_file.endswith("/run.mdp"): seedrun = "run"
    loc_cwd = os.path.dirname(mdp_file)
    
    PBS_settings["job-name"] = __base__["name"]+"_"+str(mdx)+"_"+str(seed)
    PBS_settings["cwd"] = cwd+(seedrun == "run" and f"/{seed}" or "")
    
    struc_file = os.path.relpath(struc_file, loc_cwd)
    if restart_file is not None: restart_file = os.path.relpath(restart_file, loc_cwd)
    
    commands = ""
    if restart_file: commands += f"{GMX} grompp -f {seedrun}.mdp -c {struc_file} -t {restart_file} -o {seedrun}.tpr -po {seedrun}.out.mdp "+grompp_options + "\n"
    else: commands += f"{GMX} grompp -f {seedrun}.mdp -c {struc_file} -o {seedrun}.tpr -po {seedrun}.out.mdp "+grompp_options + "\n"
    commands += f"{GMX} mdrun -deffnm {seedrun} -ntmpi $GMX_ntmpi -ntomp $GMX_ntomp "+mdrun_options + "\n"
    
    with open(f"{loc_cwd}/{seed}.pbs", "w") as fp: fp.write(PBSscript%PBS_settings + commands)
    self.jobs.append([f"{seed}.pbs", PBS_settings["cwd"]])

# generate pbs files for gmxrunner (multiple simulations per job)
def generate_gmxrunner(self, grompp_options, mdrun_options, PBS_settings, mdx, cwd):
  JOBS = []
  for seed, mdp_file, struc_file, restart_file in self.simulations:
    seedrun = seed
    if mdp_file.endswith("/run.mdp"): seedrun = "run"
    mdxLoc = mdp_file.replace(hostloc, "").split("/")[0]
    loc_cwd = os.path.dirname(mdp_file)

    struc_file = os.path.relpath(struc_file, loc_cwd)
    restart_file = os.path.relpath(restart_file, loc_cwd)

    if restart_file: grompp = f"{GMX} grompp -f {seedrun}.mdp -c {struc_file} -t {restart_file} -o {seedrun}.tpr -po {seedrun}.out.mdp "+grompp_options
    else: grompp = f"{GMX} grompp -f {seedrun}.mdp -c {struc_file} -o {seedrun}.tpr -po {seedrun}.out.mdp "+grompp_options

    job = {"cwd": cwd+(seedrun == "run" and f"/{seed}" or ""), "steps": []}
    
    job["steps"].append({"file": f"{seedrun}.tpr", "cmd": grompp})
    if seedrun == "run": job["steps"].append({"file": f"run.part*.gro", "cmd": f"{GMX} mdrun -deffnm {seedrun} -ntmpi $GMX_ntmpi -ntomp $GMX_ntomp "+mdrun_options, "tpr": True})
    else: job["steps"].append({"file": f"{seedrun}.gro", "cmd": f"{GMX} mdrun -deffnm {seedrun} -ntmpi $GMX_ntmpi -ntomp $GMX_ntomp "+mdrun_options})
    
    JOBS.append(job)
  
  os.mkdir(CWD+f"{mdxLoc}/gmxrunner/")
  
  for jobid in range(len(self.nodes)):
    node = self.nodes[jobid]
    jpn = self.jobs_per_node[jobid]
    
    h,m,s = [float(i) for i in PBS_settings["wall-time"].split(":")]
    
    yml = {"mode": "multi-cmds", "pbs-loc": f"job.pbs", "job-name": __base__["name"]+"_"+str(mdx)+"_"+str(jobid), "qsub": scheduler.gmxrunner_qsub+" job.pbs "+__compute__.get("pbs-submit-settings", ""), "scheduler": self.gmxrunner_scheduler, "wall-time-hours": h + m/60 + s/3600, "jobs": [], "active": True, "mps": self.mps, "VCP": self.VCP}
    for i in range(jpn): yml["jobs"].append(JOBS.pop(0))
    
    target = CWD+f"{mdxLoc}/gmxrunner/{jobid}/"
    target_remote = hostloc+f"{mdxLoc}/gmxrunner/{jobid}/"
    os.mkdir(target)
    
    PBS_settings["job-name"] = __base__["name"]+"_"+str(mdx)+"_"+str(jobid)
    PBS_settings["cwd"] = target_remote
    PBS_settings["node-type"] = node
    
    commands = f"python {bekkerScripts}gmxrunner.py config.yml"
    
    with open(target+"job.pbs", "w") as fp: fp.write(PBSscript%PBS_settings + commands)
    with open(target+"config.yml", "w") as fp: yaml.dump(yml, fp)
    
    self.jobs.append([f"job.pbs", target_remote])

def submit_pbs(self):
  jobs = []
  for script, cwd in self.jobs: jobs.append([script, cwd, try_submit(script, cwd, "qsub", 2)])
  self.queue = jobs

def submit_slurm(self):
  jobs = []
  for script, cwd in self.jobs: jobs.append([script, cwd, try_submit(script, cwd, "sbatch", -1)])
  self.queue = jobs
  
def submit_fugaku(self):
  jobs = []
  for script, cwd in self.jobs: jobs.append([script, cwd, try_submit(script, cwd, "pjsub", 5)])
  self.queue = jobs
    
def try_submit(script, cwd, cmd, idx):
  for i in range(25): # try 25 times and then give up
    try: p = subprocess.Popen([cmd, script]+shlex.split(__compute__.get("pbs-submit-settings", "")), stdout=subprocess.PIPE, cwd=cwd)
    except OSError: time.sleep(5)
    try:
      out, err = p.communicate()
      return out.strip().split()[idx].decode("utf-8")
    except IndexError: time.sleep(5)
  print("failed to submit job to cluster")
  exit()
    
def wait_scheduler(self, cmd, from_where):
  jobs = set([jobid for script, cwd, jobid in self.queue])
  N = 0
  while True:
    queueSet = set()
    try: p = subprocess.Popen([cmd], stdout=subprocess.PIPE)
    except OSError:
      time.sleep(1)
      continue
    out, err = p.communicate()
    for line in out.splitlines()[from_where:]:
      line = line.decode("utf-8").split()
      if line: queueSet.add(line[0])
    if len(queueSet.intersection(jobs)) == 0: break
    else: time.sleep(10)
    N += 1
  if N > 0: time.sleep(30)
    
def wait_pbs(self): wait_scheduler(self, "qstat", 2)
  
def wait_slurm(self): wait_scheduler(self, "squeue", 1)
  
def wait_fugaku(self): wait_scheduler(self, "squeue", 1)

def wait_gmxrunner(self):
  if not os.path.exists(self.queue[0][1]): return
  self.wait_internal()
  for i in range(9): 
    bad = False
    for script, cwd, jobid in self.queue:
      if not os.path.exists(cwd+"/done"): 
        bad = True
        break
    if bad: time.sleep(10)
    else: break
  
class scheduler:
  def __init__(self):
    self.reset()
    
  def reset(self):
    self.simulations = []
    self.queue = []
    self.jobs = []
    
  def addJob(self, jobid, mdp_file, struc_file, restart_file): # add simulation job to todo list
    self.simulations.append([jobid, mdp_file, struc_file, restart_file])
  
  def finalize(self, grompp_options, mdrun_options, PBS_settings, cwd, mdx, productionPhase):
    self.generate(grompp_options, mdrun_options, PBS_settings, mdx, cwd)
    self.submit()
    if productionPhase: 
      try: del RESTART["QUEUED_JOBS"]
      except: pass
    else: RESTART["QUEUED_JOBS"] = self.queue
    saveRESTART()
    if productionPhase:
      print("Production-run jobs have been submitted, fbmd is now done.")
      exit()
    self.wait()
    
  def check_and_wait(self):
    self.queue = RESTART["QUEUED_JOBS"]
    self.wait()


if not os.path.exists(CWD+"histograms"): os.mkdir(CWD+"histograms")

if "output-log" in __base__: logger = Logger(__base__["output-log"], "a")
else: logger = fauxLogger()

if os.path.exists("pid"):
  pid = open("pid").read().strip()
  try: 
    os.kill(int(pid), 0)
    print(f"process {pid} is already running...")
    raise
  except OSError: pass

open("pid", "w").write(str(os.getpid()))

Natoms = None
def getNatoms():
  global Natoms
  if Natoms is not None: return Natoms
  with open(__base__['pdb']) as f:
    f.readline()
    Natoms = int(f.readline().strip())
  return Natoms
  
gmx_performance = None
if __compute__.get("performance"): gmx_performance = __compute__["performance"]
elif __compute__.get("performance-relative"):
  "performance per 10K atoms -> normalize for this system"
  gmx_performance = (10000/getNatoms())*__compute__["performance-relative"]
  
binSize_setting = config["fbmd"].get("binSize")
if not binSize_setting: 
  binSize_setting = np.round(getNatoms()/80, -1) # automatic estimation of binSize
  if binSize_setting < 100: binSize_setting = getNatoms()/80 # automatic estimation of binSize  

if __compute__.get("gmxrunner"):
  node_configurations = __compute__["node-configs"] # name -> number_of_jobs
  VCP = __compute__.get("VCP", None)
  #VCP:
  #  cores: 8
  #  jobs-per-gpu: 6
  
  N = config["fbmd"]["seeds"]
  
  nc = list(node_configurations.items())
  nc.sort(key=lambda x: x[1], reverse=True)

  nodes = []
  while N > 0:
    sel = None
    for name, n in nc:
      if n > N: continue
      sel = name
      N -= n
      break
    if sel == None:
      print("Number of seeds is incompatible with gmxrunner on this computational cluster")
      exit()
    nodes.append(sel)
    
  # later, write a completely new queuing system that can deal with this...
  # so, some class that depending on the implementation deals with this somehow...
  
  scheduler.generate = generate_gmxrunner
  scheduler.nodes = nodes
  scheduler.jobs_per_node = [node_configurations[n] for n in nodes]
  scheduler.VCP = VCP
  scheduler.mps = __compute__.get("mps")
  scheduler.gmxrunner_scheduler = __compute__.get("gmxrunner_scheduler")  

  scheduler.wait = wait_gmxrunner
  if __compute__["compute_mode"] == "pbs":
    scheduler.submit = submit_pbs
    scheduler.wait_internal = wait_pbs
    scheduler.gmxrunner_qsub = "qsub"
  elif __compute__["compute_mode"] == "slurm":
    scheduler.submit = submit_slurm
    scheduler.wait_internal = wait_slurm
    scheduler.gmxrunner_qsub = "sbatch"
  elif __compute__["compute_mode"] == "custom":
    script = __import__(__compute__["compute_script"])
    script.init(sys.modules[__name__], scheduler)
  else:
    print("Unsupported config")
    exit()
elif __compute__["compute_mode"] == "pbs":
  scheduler.generate = generate_offload
  scheduler.submit = submit_pbs
  scheduler.wait = wait_pbs
elif __compute__["compute_mode"] == "slurm":
  scheduler.generate = generate_offload
  scheduler.submit = submit_slurm
  scheduler.wait = wait_slurm
elif __compute__["compute_mode"] == "fugaku":
  scheduler.generate = generate_offload
  scheduler.submit = submit_fugaku
  scheduler.wait = wait_fugaku
elif __compute__["compute_mode"] == "custom":
  script = __import__(__compute__["compute_script"])
  script.init(sys.modules[__name__], scheduler)
else:
  print("Unsupported config")
  exit()
  
if PBSscript is None and __compute__["compute_mode"] != "custom":
  print("compute/pbs-base was not set")
  exit()
  
# depending on settings, change job manager...
jobManager = scheduler()
  
@processify
def com_histogram(step):
  fbmd = config["fbmd"]
  ns2steps = 1e6/__base__["dt"]
  if "fixed-simulation-protocol" in fbmd: equilSteps = int(fbmd["fixed-simulation-protocol"][0]*.25*ns2steps)
  else: equilSteps = int(fbmd["simlength-initial"]*.25*ns2steps)
  if "restraints" in fbmd and "path-file" in fbmd["restraints"]:
    path = np.array([[float(j) for j in i.strip().split(",")] for i in open(CWD+config["fbmd"]["restraints"]["path-file"]).readlines()[1:]])
    vec = path[1]-path[0]
    vec /= np.linalg.norm(vec)
    
    lambdaFrom = fbmd["restraints"]["lambda-from"]*.1 - .2
    lambdaTo = fbmd["restraints"]["lambda-to"]*.1 + .2
    bins = np.arange(lambdaFrom, lambdaTo+.05, .025)
    hist = np.array([0 for i in bins])

    for i in range(len(RESTART["seeds"])):
      seed = RESTART["seeds"][i]
      if not os.path.exists(CWD+"mdx-%s/%s.cylinder.mc"%(step, seed)): continue
      
      try: COMs = np.fromfile(CWD+"mdx-%s/%s.cylinder.mc"%(step, seed), dtype=np.float32)
      except: continue # tsubame sucks
      COMs = COMs[:int(np.floor(len(COMs)/3)*3)]
      COMs = COMs.reshape((int(len(COMs)/3), 3))
      lmbd = np.tensordot(COMs-path[0], vec, axes=1)
      if step == 0: lmbd = lmbd[equilSteps:]
      
      np.add.at(hist, np.digitize(lmbd, bins)-1, 1)
    
    # clear memory
    COMs = []
    lmbd = []
      
    hist = np.log(hist/np.sum(hist).astype(np.float64))
    plt.plot(bins+((bins[1]-bins[0])*.5), hist)
    plt.suptitle("mdx-%s lambda distribution"%step)
    plt.xlabel("Lambda (nm)")
    plt.ylabel("lnP")
    plt.savefig(CWD+"histograms/mdx-%s.lambda.png"%step, dpi=600)
    plt.close()
    
@processify
def prep_histogram(step):
  fbmd = config["fbmd"]
  ns2steps = 1e6/__base__["dt"]
  if "fixed-simulation-protocol" in fbmd: equilSteps = int(fbmd["fixed-simulation-protocol"][0]*.25*ns2steps)
  else: equilSteps = int(fbmd["simlength-initial"]*.25*ns2steps)

  maxEnergyBin = -1e99
  minEnergyBin = 1e99
  count = 0
  
  bxpstats = list()
  fig, ax = plt.subplots(1,1)
  for i in range(len(RESTART["seeds"])):
    seed = RESTART["seeds"][i]
    if not os.path.exists(CWD+"mdx-%s/%s.fb.ene"%(step, seed)): continue
    
    try: single = np.fromfile(CWD+"mdx-%s/%s.fb.ene"%(step, seed), dtype=np.float32)
    except: continue # tsubame sucks
    if step == 0: single = single[equilSteps:]
    if len(single) == 0: continue # tsubame sucks

    bxpstats.extend(mpl.cbook.boxplot_stats(single, labels=[seed]))
    
    mn = np.min(single)
    mx = np.max(single)
    if mn < minEnergyBin: minEnergyBin = mn
    if mx > maxEnergyBin: maxEnergyBin = mx
    count += len(single)
    
  # clear memory
  single = []
  ax.bxp(bxpstats, vert=0)
  plt.suptitle("mdx-%s potential energy range plot"%step)
  plt.xlabel("Potential energy (kj/mol)")
  plt.savefig(CWD+"histograms/mdx-%s.erp.png"%step, dpi=600) # the epots histogram is only based on mdx-X's potentials
  plt.close()
  
  return minEnergyBin, maxEnergyBin, count
  
@processify
def prep_histogram_fast(step):
  fbmd = config["fbmd"]
  ns2steps = 1e6/__base__["dt"]
  if "fixed-simulation-protocol" in fbmd: equilSteps = int(fbmd["fixed-simulation-protocol"][0]*.25*ns2steps)
  else: equilSteps = int(fbmd["simlength-initial"]*.25*ns2steps)

  maxEnergyBin = -1e99
  minEnergyBin = 1e99
  count = 0
  
  for i in range(len(RESTART["seeds"])):
    seed = RESTART["seeds"][i]
    if not os.path.exists(CWD+"mdx-%s/%s.fb.ene"%(step, seed)): continue
    
    single = np.fromfile(CWD+"mdx-%s/%s.fb.ene"%(step, seed), dtype=np.float32)
    if single is None: continue
    if step == 0: single = single[equilSteps:]
    if len(single) == 0: continue # tsubame sucks

    mn = np.min(single)
    mx = np.max(single)
    if mn < minEnergyBin: minEnergyBin = mn
    if mx > maxEnergyBin: maxEnergyBin = mx
    count += len(single)
  
  return minEnergyBin, maxEnergyBin, count

@processify
def load_histogram(step, seeds, bins, binSize):
  fbmd = config["fbmd"]
  ns2steps = 1e6/__base__["dt"]
  if "fixed-simulation-protocol" in fbmd: equilSteps = int(fbmd["fixed-simulation-protocol"][0]*.25*ns2steps)
  else: equilSteps = int(fbmd["simlength-initial"]*.25*ns2steps)
  
  minEnergyBin = bins[0]
  hist = np.array([0 for i in bins])
  arr = np.array([[0 for i in bins] for j in bins], dtype=np.int32)
  
  for seed in seeds:
    if not os.path.exists(CWD+"mdx-%s/%s.fb.ene"%(step, seed)): continue
    
    single = np.fromfile(CWD+"mdx-%s/%s.fb.ene"%(step, seed), dtype=np.float32)
    if step == 0: single = single[equilSteps:]
    #single = single[np.isfinite(single)]
    
    indexes = np.floor((single-minEnergyBin)/binSize).astype(np.int32) # np.digitize is slower
    
    np.add.at(hist, indexes, 1)
    np.add.at(arr, (indexes[:-1], indexes[1:]), 1)
  
  return hist, arr
  
def chunkList(l, n):
  for i in range(0, len(l), n): yield l[i:i+n]
  
def buildHistogram(step):
  fbmd = config["fbmd"]
  
  initialTemp = float(config["fbmd"]["initialTemp"])
  minTemp = float(config["fbmd"]["minTemp"])
  maxTemp = float(config["fbmd"]["maxTemp"])
  minSamples = float(config["fbmd"]["minSamples"])
  convVal =  0.008314462175 # kj/(mol*K)
  binSize = binSize_setting
  
  ns2steps = 1e6/__base__["dt"]
  if "fixed-simulation-protocol" in fbmd: equilSteps = int(fbmd["fixed-simulation-protocol"][0]*.25*ns2steps)
  else: equilSteps = int(fbmd["simlength-initial"]*.25*ns2steps)
  baka = np.array([initialTemp/minTemp, initialTemp/maxTemp], dtype=np.float32) # gamma itself was saved in single precision, so the comparison must be made using single precision data, else it doesn't match

  # execute this in a new process...
  for i in range(10):
    try:
      if "--extensive-plotting" in sys.argv:
        if "restraints" in fbmd and "path-file" in fbmd["restraints"]: com_histogram(step)
        minEnergyBin, maxEnergyBin, count = prep_histogram(step)
      else: minEnergyBin, maxEnergyBin, count = prep_histogram_fast(step)
    except: count = 0
    if count > 0: break

  if step > 0:
    gfp = open(CWD+"histograms/mdx-%s.dat"%(step-1,), "rb")
    chunk = gfp.read(12)
    data = struct.unpack("=ffi", chunk)
    alt_bins = [data[0]]
    binSize = data[1]
    for i in range(data[2]-1): alt_bins.append(alt_bins[i]+binSize)
    alt_gamma = struct.unpack("f"*data[2], gfp.read(4*data[2]))
    alt_gamma = np.array(alt_gamma, dtype=np.float32)
    
    minEnergyBin = float(int(minEnergyBin)-(int(minEnergyBin)%binSize))
    maxEnergyBin = float(int(maxEnergyBin)-(int(maxEnergyBin)%binSize))
    
    bins = list(np.arange(minEnergyBin, maxEnergyBin+binSize, binSize))
    bins = list(np.arange(min(bins+alt_bins), max(bins+alt_bins)+binSize+binSize, binSize))
    
    gamma = [None for i in range(len(bins))]

    for i in range(len(alt_gamma)): gamma[bins.index(alt_bins[i])] = alt_gamma[i]
    
    x = -1
    for i in range(len(gamma)):
      if gamma[i] != None: 
        x = i
        break
        
    for i in range(0, x): gamma[i] = gamma[x]
    
    x = -1
    for i in reversed(range(len(gamma))):
      if gamma[i] != None: 
        x = i
        break
    for i in range(x, len(gamma)): gamma[i] = gamma[x]
    
    bins = np.array(bins, dtype=np.float32)
  else:
    minEnergyBin = float(int(minEnergyBin)-(int(minEnergyBin)%binSize))
    maxEnergyBin = float(int(maxEnergyBin)-(int(maxEnergyBin)%binSize))
    bins = np.arange(minEnergyBin, maxEnergyBin+binSize+binSize, binSize)
    gamma = [initialTemp/fbmd.get("center-temp", fbmd["maxTemp"]) for i in range(len(bins))]

  # execute this in a new process... (multiple if there is a large amount of data to process on annoying T4)
  
  seedList = RESTART["seeds"]
  hist, arr = None, None
  for subset in chunkList(seedList, np.ceil(len(seedList)/(count/1e9)).astype(int)):
    hist_, arr_ = load_histogram(step, subset, bins, binSize)
    if hist is None:
      hist = np.array(hist_)
      arr = np.array(arr_)
    else:
      hist += np.array(hist_)
      arr += np.array(arr_)

  Dgamma = np.array([0.0 for i in gamma], dtype=np.float32)
  
  hist_pdf = hist/float(np.sum(hist))
  hist_pdf_log = np.log(hist_pdf)
    
  bincenters = bins+((bins[1]-bins[0])*.5)
  plt.suptitle("mdx-%s potential energy distribution plot"%step)
  plt.xlabel("Potential energy (kj/mol)")
  plt.ylabel("ln(P)")
  plt.plot(bincenters, hist_pdf_log, linewidth=.33)
  plt.savefig(CWD+"histograms/mdx-%s.edpf.png"%step, dpi=600) # the epots histogram is only based on mdx-X's potentials
  plt.close()
  
  hist_pdf_log[hist_pdf_log == -np.inf] = np.min(hist_pdf_log[hist_pdf_log != -np.inf])
  
  tmp = np.mean(hist)*.01
  if tmp > minSamples: minSamples = tmp
  if np.max(hist) < minSamples: minSamples = int(np.mean(hist))
  
  extrap_weights = []
  
  # figure out to which bins the bins progress to...
  extrap = [0]
  
  for i in range(1, len(hist)-1):
    npre = len(np.where(arr[i][:i] > 0)[0])
    npost = len(np.where(arr[i][i+1:] > 0)[0])
    N = max(npre, npost)
    totgrad = 0.0
    totratio = 0.0
    nox = hist[i]
    if nox:
      if hist[i-1] + hist[i+1]:
        ratio = (float(arr[i][i-1] + arr[i][i+1]) / (hist[i-1] + hist[i+1])) * (float(arr[i][i-1] + arr[i][i+1]) / nox)
        totgrad += ((hist_pdf_log[i+1] - hist_pdf_log[i-1]) / (2.0*binSize)) * ratio
        totratio += ratio

      for j in range(i-N+1, i):
        dl = i-j
        if i-dl < 0 or i+dl > len(hist_pdf_log)-1 or not hist[i-dl] + hist[i+dl]: continue
        ratio = (float(arr[i][i-dl] + arr[i][i+dl]) / (hist[i-dl] + hist[i+dl])) * (float(arr[i][i-dl] + arr[i][i+dl]) / nox)
        totgrad += ((hist_pdf_log[i+dl] - hist_pdf_log[i-dl]) / (dl*2.0*binSize)) * ratio
        totratio += ratio
      if totratio > 0.0: totgrad /= totratio

    Dgamma[i] = convVal * initialTemp * totgrad
    if hist[i-1] > minSamples and hist[i+1] > minSamples: extrap_weights.append(1.0)
    else:
      extrap.append(i)
      extrap_weights.append(min(hist[i-1], hist[i+1])/minSamples)
  extrap.append(len(hist)-1)
  
  old_bins = np.copy(bins)
  old_gamma = np.copy(gamma)
  
  Dgamma[0] = ((convVal * initialTemp * (hist_pdf_log[1] - hist_pdf_log[0])) / (2.0 * binSize))
  Dgamma[-1] = ((convVal * initialTemp * (hist_pdf_log[-1] - hist_pdf_log[-2])) / (2.0 * binSize))
  extrap_weights.insert(0, min(hist[0], hist[1])/minSamples)
  extrap_weights.append(min(hist[-1], hist[-2])/minSamples)
    
  bincenters_old = bincenters

  try: fi_ = np.where((old_gamma) >= baka[0])[0][-1]
  except: fi_ = 0
  try: ti_ = np.where(old_gamma <= baka[1])[0][0]
  except: ti_ = len(old_gamma)-1

  stddev, coverage = np.std(hist_pdf_log[fi_:ti_]), float((initialTemp/gamma[ti_]) - (initialTemp/gamma[fi_])) / (maxTemp - minTemp)
  if np.abs(coverage-1.0) < 1e-4: coverage = 1.0
  
  hasDoneExtrapolation = False
  
  sf = 1.0
  if step >= RESTART.get("flat-condition", np.inf): sf = max(stddev/fbmd["min-stddev"], 1.0)
  Dgamma *= sf

  if coverage >= 1.0 and np.min(hist[fi_:ti_]) > minSamples: 
    Dgamma[np.array(extrap_weights, dtype=np.float32) < 1] = 0.0 # ignore any bins which have less than /minSamples/ number of samples after obtaining a somewhat flat distribution
  else: # extrapolation & smoothing of low-density data during the initial training phase (before obtaining a somewhat flat distribution)
    gamma_pre = gamma + Dgamma

    FP = np.where(old_gamma == baka[0])[0]
    FP = len(FP) and FP[-1] or -1
    LP = np.where(old_gamma == baka[1])[0]
    LP = len(LP) and LP[0] or 1e99

    order = 2

    if coverage < 1.0: # only extrapolate while the coverage < 100%
      mask = np.ones(gamma_pre.shape,dtype=bool) #np.ones_like(a,dtype=bool)
      mask[extrap] = False
      noe = int(np.round(fbmd.get("extrap-percentage", 0.0) * .01 * 0.5 * (np.abs(baka[1] - baka[0]) / (np.abs(min(gamma_pre[mask]) - max(gamma_pre[mask])) / np.abs(np.argmax(gamma_pre[mask]) - np.argmin(gamma_pre[mask]))))))

      # don't make it extrapolate too much in the beginning when there is not enough data...
      tmp = int(np.round(len(np.where(np.array(extrap_weights, dtype=np.float32) >= 1.0)[0])*.5))

      if noe > tmp: noe = tmp
      
      noeA = np.max([noe-np.where(hist > 0)[0][0], 0])
      noeB = np.max([noe-(len(hist)-np.where(hist > 0)[0][-1]), 0])
      
      if noeA > 0 or noeB > 0: hasDoneExtrapolation = True

      for i in range(len(extrap)): extrap[i] += noeA
      Dgamma = list(Dgamma)
      gamma = list(gamma)
      bins = list(bins)

      for i in range(noeA):
        Dgamma.insert(0, 0.0)
        extrap_weights.insert(0, 0.0)
        extrap.insert(0, i)
        gamma.insert(0, gamma[0])
        bins.insert(0, bins[0]-binSize)
        
      for i in range(noeB):
        Dgamma.append(0.0)
        extrap_weights.append(0.0)
        extrap.append(len(Dgamma)-1)
        gamma.append(gamma[-1])
        bins.append(bins[-1]+binSize)
        
      Dgamma = np.array(Dgamma, dtype=np.float32)
      gamma = np.array(gamma, dtype=np.float32)
      bins = np.array(bins, dtype=np.float32)
      
    gamma_pre = gamma + Dgamma
      
    B = bins[:-1]+(binSize*.5)
    G = np.copy(gamma_pre)
    W = np.array(extrap_weights, dtype=np.float32)
      
    mask = np.ones(gamma_pre.shape,dtype=bool) #np.ones_like(a,dtype=bool)
    mask[W < 1] = False
    N = int(.2 * (np.abs(baka[1] - baka[0]) / (np.abs(min(gamma_pre[mask]) - max(gamma_pre[mask])) / np.abs(np.argmax(gamma_pre[mask]) - np.argmin(gamma_pre[mask])))))
    if N > len(mask): order = 1

    bc = bins + (binSize*.5)
      
    for i in extrap:
      if i < FP and extrap_weights[i] < 1: continue
      if i > LP and extrap_weights[i] < 1: continue
      dsts = np.abs(B-bc[i]) # find the nearest bins
      dgroup = dsts.argsort()
      dgroup = dgroup[W[dgroup] > 0.0] #everything with W =< 0.0 should be excluded --> these are the extrapolated bins (with no information)
      xtra = len(np.where(W[dgroup[:N]] < 1.0)[0]) # make sure that there is enough high quality data...
      dgroup = dgroup[:N+xtra] # only use the local data
      polyFunc = np.poly1d(np.polyfit(B[dgroup], G[dgroup], order, w=W[dgroup])) # extrapolate using a 2nd order polynomial
      Dgamma[i] = (((polyFunc(bc[i])-gamma[i])*(1-extrap_weights[i])) + (Dgamma[i] * extrap_weights[i])) # and reweight between extrapolated data and measured data based on the estimated accuracy of the measured data

    if fbmd.get("extrap-percentage", 0.0) == 0: Dgamma[extrap_weights == 0] = 0
    bins = np.array(bins, dtype=np.float32)
    gamma = np.array(gamma, dtype=np.float32)
    Dgamma = np.array(Dgamma, dtype=np.float32)
    bincenters = 0.5*(bins[1:]+bins[:-1])

  for i in range(len(Dgamma)): gamma[i] += Dgamma[i]

  gamma = np.array(gamma, dtype=np.float32)
  
  try: fi_p = np.where(gamma >= baka[0])[0][-1]
  except: fi_p = 0
  try: ti_p = np.where(gamma <= baka[1])[0][0]+1
  except: ti_p = len(gamma)-1
  
  if gamma[-1] < baka[0]: gamma[np.where(gamma > baka[0])[0]] = baka[0]
  else: fi_p = 0
  if gamma[0] > baka[1]: gamma[np.where(gamma < baka[1])] = baka[1]
  else: ti_p = len(gamma)-1
  if ti_p > len(gamma)-1: ti_p = len(gamma)-1
  
  plt.suptitle("mdx-%s potential energy distribution plot"%step)
  plt.xlabel("Potential energy (kj/mol)")
  plt.ylabel("ln(P)")
  
  stddev = np.std(hist_pdf_log[fi_:ti_])
  coverage = float((initialTemp/gamma[ti_p]) - (initialTemp/gamma[fi_p])) / (maxTemp - minTemp)
  if np.abs(coverage-1.0) < 1e-4: coverage = 1.0
  logger.write("step: %s coverage: %s stddev: %s\n"%(step, coverage, stddev))
  
  if hasDoneExtrapolation: plt.plot(bincenters_old, hist_pdf_log, linewidth=.33)
  else: plt.plot(bincenters_old[fi_:ti_], hist_pdf_log[fi_:ti_], linewidth=.33)
  plt.savefig(CWD+"histograms/mdx-%s.edpf.test.png"%step, dpi=600) # the epots histogram is only based on mdx-X's potentials
  plt.close()
  
  plt.suptitle("mdx-%s force bias plot"%step)
  plt.xlabel("Energy (kj/mol)")
  plt.ylabel("$\Delta$$\gamma$")
  
  plt.plot(bincenters[fi_p:ti_p], Dgamma[fi_p:ti_p], linewidth=.33)
  plt.savefig(CWD+"histograms/mdx-%s.dgamma.png"%step, dpi=600)
  plt.close()
  
  plt.suptitle("mdx-%s force bias plot (int)"%step)
  plt.xlabel("Energy (kj/mol)")
  plt.ylabel("$\Delta$$\gamma$")
  
  plt.plot(bincenters[fi_p:ti_p], np.cumsum(Dgamma[fi_p:ti_p]), linewidth=.33)
  plt.savefig(CWD+"histograms/mdx-%s.dgamma-cs.png"%step, dpi=600)
  plt.close()

  sz = np.ceil((ti_p-fi_p)*0.025).astype(np.int32)
  fi_p = max(fi_p-sz, 0)
  ti_p = min(ti_p+sz, len(gamma)-1)
  gamma = gamma[fi_p:ti_p]
  bins = bins[fi_p:ti_p]

  plt.suptitle("mdx-%s force bias plot"%step)
  plt.xlabel("Energy (kj/mol)")
  plt.ylabel("$\gamma$")
  
  bincenters = bins+((bins[1]-bins[0])*.5)
  plt.plot(bincenters, gamma, linewidth=.33)
  plt.savefig(CWD+"histograms/mdx-%s.gamma.png"%step, dpi=600)
  plt.close()
  
  data = [bins[0], binSize, len(gamma)]+list(gamma)
  open(CWD+"histograms/mdx-%s.dat"%step, "wb").write(struct.pack("=ffi"+("f"*len(gamma)), *data))
  
  if "--preview" in sys.argv: return stddev, coverage, np.array(bins, dtype=np.float32), gamma, binSize
  
  return stddev, coverage, np.array(bins, dtype=np.float32), gamma, binSize

# run the next iteration (automatically determine the histogram mode)
def mdXrun(step):
  loadConfig()
  
  fbmd = config["fbmd"]
  
  md = loadMD(open(CWD+__base__["base_inp"]).read())
  
  initialTemp = float(config["fbmd"]["initialTemp"])
  
  flatConditionAchieved = (RESTART.get("flat-condition") or 1e99) < step
  stableCondition = (RESTART.get("stable-condition") or 1e99) < step
  
  productionPhase = (RESTART.get("production-phase") or 1e99) < step
  
  if productionPhase and os.path.exists("prod"):
    print("Production run already done/initiated.")
    exit()
  
  # set restraints if required
  # both protein distance restraints & ligand - pocket distance restraints
  
  if "restraints" in fbmd and step > -1:
    if "posres-file" in fbmd["restraints"]:
      md["do-bekker-posres"] = "yes"
      md["bekker-posres-file"] = hostloc + fbmd["restraints"]["posres-file"]
    if "fb-posres-file" in fbmd["restraints"]:
      md["fb-posres-file"] = hostloc + fbmd["restraints"]["fb-posres-file"]
      md["fb-posres-lr"] = 500000
    if "disres-file" in fbmd["restraints"]:
      md["do-bekker-disres"] = "yes"
      md["bekker-disres-file"] = hostloc + fbmd["restraints"]["disres-file"]
    if "comres-file" in fbmd["restraints"]:
      md["do-bekker-comres"] = "yes"
      md["bekker-comres-file"] = hostloc + fbmd["restraints"]["comres-file"]
      if "comres-particle-k" in fbmd["restraints"]: md["comres-particle-k"] = fbmd["restraints"]["comres-particle-k"]
      if "comres-box-dmz" in fbmd["restraints"]: 
        md["comres-box-dmz"] = fbmd["restraints"]["comres-box-dmz"]
        if "comres-box-dmz-dims" in fbmd["restraints"]: md["comres-box-dmz-dims"] = fbmd["restraints"]["comres-box-dmz-dims"]
    if "path-file" in fbmd["restraints"]:
      md["do-bekker-cylinder"] = "yes"
    
      md["cylinder-group"] = fbmd["restraints"]["restrain-group"]
      md["cylinder-path-file"] = hostloc+fbmd["restraints"]["path-file"]
    
      md["cylinder-max-orth-dist"] = fbmd["restraints"]["orthogonal-range"]*.1
      if "orthogonal-range2" in fbmd["restraints"]: md["cylinder-max-orth-dist2"] = fbmd["restraints"]["orthogonal-range2"]*.1
      if "cylinder-out-steps" in fbmd["restraints"]: md["cylinder-out-steps"] = fbmd["restraints"]["cylinder-out-steps"]
      
      md["cylinder-orth-k"] = fbmd["restraints"]["orthogonal-force"]*CAL*100
    
      md["cylinder-lambda-range"] = "%s %s"%(fbmd["restraints"]["lambda-from"]*.1, fbmd["restraints"]["lambda-to"]*.1)
      md["cylinder-lambda-k"] = fbmd["restraints"]["lambda-force"]*CAL*100

  if step == -1:
    random.seed(fbmd["seeds-seed"])
    RESTART["real-seeds"] = random.sample(range(int(1e4), int(1e7)), fbmd["seeds"])
    RESTART["seeds"] = list(range(fbmd["seeds"]))
  
    loopli = .1 # 100 ps
    md["define"] = "-DPOSRES" # use position restraints during equilibration of the velocities
    coverage = 0.0
    
    RESTART["coverages"] = {}
    RESTART["stddevs"] = {}
  elif step == 0:
    if "fixed-simulation-protocol" in config["fbmd"]: loopli = config["fbmd"]["fixed-simulation-protocol"][0]
    else: loopli = fbmd["simlength-initial"]
    coverage = 0.0
  else:
    if "--quick-resume" in sys.argv and len(RESTART["stddevs"]) == step:
      stddev = RESTART["stddevs"][step-1]
      coverage = RESTART["coverages"][step-1]
      refLL = None
    else: 
      stddev, coverage, bins, gamma, binSize = buildHistogram(step-1)
      RESTART["coverages"][step-1] = float(coverage)
      RESTART["stddevs"][step-1] = float(stddev)
      refLL = (len(bins)*fbmd["minSamples"]*__base__["dt"]*.25)/1e6
    md["fb-histogram-file"] = hostloc+"histograms/mdx-%s.dat"%(step-1)
    if "fixed-simulation-protocol" in config["fbmd"]:
      if step < len(config["fbmd"]["fixed-simulation-protocol"]): loopli = config["fbmd"]["fixed-simulation-protocol"][step]
      else: 
        productionPhase = True # start the production phase after the fixed protocol has finished
        RESTART["production-phase"] = step
    else:
      loopli = config["fbmd"]["simlength"] * coverage
    
      keys = RESTART["coverages"].keys()
      coverages = np.array([RESTART["coverages"][key] for key in keys])
      stddevs = np.array([RESTART["stddevs"][key] for key in keys])
      
      if stddev < 1.0: # do shorter simulations when the distribution is less flat (to more quickly update the bias)
        fastsim = loopli * .5**4 # lower
        longsim = loopli  # max
      
        A = 1.0
        B = config["fbmd"]["min-stddev"]
        
        if len(coverages) > 3 and coverages[-2] < 1.0 and stddev < B*2: longsim = loopli * .25 # make sure that the simulation length doesn't immediatly go to max after reaching full coverage...
    
        ratio = (stddev - B) / (A-B) # stddev:1.0 --> ratio:1.0 ; stddev:0.2 --> ratio:0.0
        ratio = 1-ratio # stddev:1.0 --> ratio:0.0 ; stddev:0.2 --> ratio:1.0
        ratio **= 2
      
        loopli = ((longsim-fastsim)*ratio) + fastsim
      
        if loopli > longsim: loopli = longsim
        elif loopli < fastsim: loopli = fastsim
      if coverage < 1.0 or stddev > 1.0: loopli = (config["fbmd"]["simlength"] * coverage) * .5**4
    
      if refLL is not None and loopli < refLL: loopli = refLL
  
  if not "fixed-simulation-protocol" in config["fbmd"] and len(np.where(np.array(list(RESTART["coverages"].values())) >= 1.0)[0]) > 5:
    # but before moving to the production phase, make sure that the previous iteration was at least full length (i.e. 16ns)
    if not productionPhase and not flatConditionAchieved:
      if coverage >= 1.0 and RESTART["stddevs"].get(np.max(list(RESTART["stddevs"].keys()))-1, np.inf) < config["fbmd"]["min-stddev"] and stddev < config["fbmd"]["min-stddev"]:
        flatConditionAchieved = True
        RESTART["flat-condition"] = step
      elif step >= fbmd.get("go-anyway-step", np.inf):
        flatConditionAchieved = True
        RESTART["flat-condition"] = step
  
    if not productionPhase and not stableCondition and flatConditionAchieved: # do long simulations in order to do equilibration...
      if step-RESTART["flat-condition"] >= fbmd.get("mcmd-equil-stages", 0):
        stableCondition = True
        RESTART["stable-condition"] = step
      else: loopli = fbmd.get("mcmd-equil-simlength", config["fbmd"]["production"]["simlength"])
  
    if not productionPhase and stableCondition and flatConditionAchieved:
      productionPhase = True
      RESTART["production-phase"] = step

  use_restart = [step-1 for i in RESTART["seeds"]]
  
  if productionPhase:
    loopli = config["fbmd"]["production"]["simlength"]
    if "save-xtc-group" in config["fbmd"]["production"]: # save as xtc
      md["compressed-x-grps"] = config["fbmd"]["production"]["save-xtc-group"]
      md["nstxout-compressed"] = int(np.round(config["fbmd"]["production"]["trj-save-fs"] / __base__["dt"]))
    else: # save as trr
      md["nstxout"] = int(np.round(config["fbmd"]["production"]["trj-save-fs"] / __base__["dt"]))
    if "cylinder-out-steps" in md: md["cylinder-out-steps"] = int(np.round(config["fbmd"]["production"]["trj-save-fs"] / __base__["dt"]))
  else: 
    RESTART["production-phase"] = None
  
  wt_nod = gmx_performance is not None and float(loopli/gmx_performance) or None
  
  ns2steps = 1e6/__base__["dt"]
  loopli = int(np.round(loopli*ns2steps))
  
  md["continuation"] = "yes"
  md["gen-vel"] = "no"
  
  md["dt"] = __base__["dt"]/1000.0
  md["ref_t"] = " ".join([str(initialTemp) for i in md["ref_t"].split()])
  
  md["do-fb-mcmd"] = "yes"
  
  if "restraints" in fbmd and fbmd["restraints"].get("disres-scaling") and step > -1: md["fb-disres-scaling"] = "yes"
  
  if fbmd.get("include-restrain-epot"): md["fb-include-restrain-epot"] = "yes"
  
  if productionPhase:
    if not "--production-run" in sys.argv:
      print("Production run flag not added.")
      exit()
  
  md["fb-base-temp"] = initialTemp

  if productionPhase:
    mdxDir = CWD+f"prod/"
    
    os.mkdir(CWD+"prod")
    os.chdir(CWD+"prod") 
  else:
    mdxDir = CWD+f"mdx-{step}/"
    mdxDir_remote = hostloc+f"mdx-{step}/"
    os.mkdir(CWD+f"mdx-{step}")
    os.chdir(CWD+f"mdx-{step}")
  
  # reset job manager
  jobManager.reset()
  
  for seed in RESTART["seeds"]:
    if productionPhase: mdxDir_remote = hostloc+f"prod/{seed}/"
    md["nsteps"] = loopli
    
    if step > 0:
      testStep = use_restart[seed] # to allow switching back to a stable state of this simulation...
      prev = False
      while True: # find the latest cpt file... --> needs to be done to deal with bad systems (i.e. CMC's VCC, tsubame)
        if os.path.exists(CWD+"mdx-%s/%s.cpt"%(testStep, seed)) and os.stat(CWD+"mdx-%s/%s.cpt"%(testStep, seed)).st_size > 0: break
        if os.path.exists(CWD+"mdx-%s/%s_prev.cpt"%(testStep, seed)) and os.stat(CWD+"mdx-%s/%s_prev.cpt"%(testStep, seed)).st_size > 0: 
          prev = True
          break
        testStep -= 1
      if testStep >= 0: 
        if prev: restart_file = hostloc+"mdx-%s/%s_prev.cpt"%(testStep, seed)
        else: restart_file = hostloc+"mdx-%s/%s.cpt"%(testStep, seed)
      else: 
        logger.write("Missing restart file for mdx-%s/%s\n"%(testStep, seed))
        exit()
      
    elif step == -1: # equilibrate the velocities with position restraints using -DPOSRES
      if "cpt" in __base__:  restart_file = hostloc+__base__["cpt"]%{"seed": seed} # load a restart file to find accurate box information
      else: restart_file = None
      md["gen-vel"] = "yes"
      md["gen-seed"] = RESTART["real-seeds"][seed]
      md["continuation"] = "no"
      md["gen-vel"] = "yes"
      md["gen-temp"] = initialTemp
      
      try: del md["do-fb-mcmd"]
      except: pass
      try: del md["fb-include-restrain-epot"]
      except: pass
      try: del md["fb-base-temp"]
      except: pass
      try: del md["nstcalcenergy"]
      except: pass
      
    elif step == 0: # equilibrate the structures with McMD
      restart_file = hostloc+"mdx-%s/%s.cpt"%(-1, seed)
      md["fb-heating-temp"] = fbmd.get("center-temp", fbmd["maxTemp"])
    
    if productionPhase:
      os.mkdir(f"{seed}")
      with open(CWD+f"prod/{seed}/run.mdp", "w") as fp: fp.write(saveMD(md))
      jobManager.addJob(seed, hostloc+f"prod/{seed}/run.mdp", hostloc+__base__["pdb"], restart_file)
    else:
      with open(CWD+f"mdx-{step}/{seed}.mdp", "w") as fp: fp.write(saveMD(md))
      jobManager.addJob(seed, hostloc+f"mdx-{step}/{seed}.mdp", hostloc+__base__["pdb"], restart_file)
    
  index_file = __base__.get("index")
  
  PBS_settings = dict(__compute__.get("pbs-settings", {}))
  grompp_options = [f"-p {os.path.relpath(hostloc+__base__['tpl'], mdxDir_remote)}"]
  mdrun_options = list(__compute__.get("mdrun-options", ["-update cpu"]))

  if wt_nod is not None:
    if productionPhase: wt_secs = 24*3600 # 24H
    else: 
      wt_secs = int(np.round((wt_nod*24*60*60*1.1)+300)) # walltime in days to seconds + 10% + 5 min
      mdrun_options.append(f"-maxh {wt_secs/3600.}")
    PBS_settings["wall-time"] = "%02d:%02d:%02d"%((wt_secs // (3600 * 24))*24 + ((wt_secs // 3600) % 24), (wt_secs // 60) % 60, wt_secs % 60)

  if index_file: grompp_options.append(f"-n {os.path.relpath(hostloc+index_file, mdxDir_remote)}")
  if step == -1: grompp_options.append(f"-r {os.path.relpath(hostloc+__base__['pdb'], mdxDir_remote)}")
  nwarn = __base__.get("nwarn", 0)
  if nwarn: grompp_options.append("-maxwarn %s"%nwarn)

  if productionPhase: jobManager.finalize(" ".join(grompp_options), " ".join(mdrun_options), PBS_settings, CWD+f"prod/", step, True)
  else: jobManager.finalize(" ".join(grompp_options), " ".join(mdrun_options), PBS_settings, CWD+f"mdx-{step}/", step, False)
  
  if not productionPhase or step < RESTART["production-phase"]+config["fbmd"]["production"].get("phases", 1)-1: 
    if __compute__["compute_mode"] == "fugaku": time.sleep(30)
    if os.environ.get("FBMD_RESTART") == "1": restart_program()
    else: mdXrun(step+1)

folders = [i for i in os.listdir(CWD) if os.path.isdir(CWD+i)]
files = [i for i in os.listdir(CWD) if not os.path.isdir(CWD+i)]
x = -2
for i in folders:
  if i[:4] == "mdx-" and int(i[4:]) > x: x = int(i[4:])

if "QUEUED_JOBS" in RESTART: jobManager.check_and_wait()

mdXrun(x+1)
