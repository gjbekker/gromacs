import numpy as np, mdtraj, os, yieldxtc, subprocess, itertools, sys

import tools

def additionalArguments(optparser):
  optparser.add_argument("--fix-ligand", action="store_true")

tools.setup(additionalArguments)

tools.initStructure()

Nbakapara = tools.config["fbmd"]["seeds"]

outputIndexes = tools.outputIndexes() # outIndexes receptorCalpha receptorAtoms ligandAtoms

proteinAtoms = outputIndexes.receptorAtoms
ligAtoms = outputIndexes.ligandAtoms

ref = tools.load(tools.CWD+tools.config["base"]["pdb"])
ref.atom_slice(outputIndexes.raw, inplace=True)

indicesSI = outputIndexes.receptorCalpha
if tools.config["analysis"].get("si-posres"):
  prfile = tools.config["fbmd"].get("restraints", {}).get("posres-file")
  if not prfile: 
    print("No position restraints used, while si-posres was set...")
    exit()
  indicesSI = []
  for line in open(tools.CWD+prfile).readlines()[1:]:
    if not line.strip(): continue
    line = line.split()
    indicesSI.append(np.where(int(line[0]) == outputIndexes.raw)[0][0])
  indicesSI = np.array(indicesSI)
  print(len(indicesSI), "atoms selected for superposition")

struc = tools.baseStruct.atom_slice(outputIndexes.raw, inplace=False)
struc.superpose(ref, 0, indicesSI)

struc.unitcell_vectors = ref.unitcell_vectors
struc.unitcell_angles = ref.unitcell_angles

fileinfo = tools.findProdData()

if not os.path.exists(tools.analFolder+"cleaned"): os.mkdir(tools.analFolder+"cleaned")
if not os.path.exists(tools.analFolder+"ref.gro"): struc.save_gro(tools.analFolder+"ref.gro")

dt = tools.config["base"].get("dt", 2)*.001

def prepAux(): # also make this thing prepare .lambda files...
  nstxout = int(tools.config["fbmd"]["production"]["trj-save-fs"]/tools.config["base"]["dt"])
  
  for seed in range(Nbakapara):
    seed = str(seed)
    if os.path.exists(tools.analFolder+"cleaned/%s.fb.ene"%seed): continue
    info = sorted(fileinfo[seed].items(), key=lambda i: i[1])
    
    N = 0
    allEne = []
    for i, (file, startTime) in enumerate(info):
      ene = np.fromfile(file.replace(".xtc", ".fb.ene"), dtype=np.float32)
      if i < len(info)-1: 
        nextTime = info[i+1][1]
        nsteps = int((nextTime-startTime)/dt)+1
        if len(ene) <= nsteps: nsteps -= 1
        ene = ene[:nsteps]
        if len(ene): allEne.append(ene)
      elif len(ene): allEne.append(ene)

    ene = np.concatenate(allEne)
    ene = ene[::nstxout]
    ene.tofile(tools.analFolder+"cleaned/%s.fb.ene"%seed)
    print(seed, len(ene))

def chunkList(l, n):
  for i in range(0, len(l), n): yield l[i:i+n]

def saveIndex(index, fn):
  out = []
  for k,v in index.items():
    out.append("[ %s ]"%k)
    for sl in chunkList(v, 15): out.append(" ".join([str(i+1) for i in sl]))
  open(fn, "w").write("\n".join(out)+"\n")

def cleanFile(file):
  if os.path.exists(file): os.remove(file)

def fixXTC():
  projectName = tools.config["base"]["name"]

  gmx = tools.config["compute"]["gmx"]

  tools.index["cleanedAtoms"] = outputIndexes.raw
  saveIndex(tools.index, tools.analFolder+"temp/index.temp.ndx")
  
  pairs = np.array(list(itertools.product(ligAtoms, proteinAtoms)), dtype=np.int32)
  xyzs = np.zeros((1, 2, 3), dtype=np.float32)
  displacement = np.zeros((1, 1, 3), dtype=np.float32)
  fake_pairs = np.array([[1, 0]], dtype=np.int32)
  
  custom_pbc_fix = tools.config["analysis"].get("custom-pbc-fix")
  if custom_pbc_fix is not None: custom_pbc_fix = __import__(tools.config["analysis"]["custom-pbc-fix"]).fix_pbc
  
  for seed in range(Nbakapara):
    seed = str(seed)
    info = sorted(fileinfo[seed].items(), key=lambda i: i[1])
    if os.path.exists(tools.analFolder+"cleaned/%s.xtc"%seed): continue
      
    procs = []
    for i, (file, startTime) in enumerate(info):
      cleanFile("/tmp/mcmd_processing_%s_%s.tmp.xtc"%(projectName, i))
      if i < len(info)-1: nextTime = info[i+1][1]
      else: nextTime = np.inf

      tpr = file.split(".part")[0].replace(".xtc", "")+".tpr"
      if os.path.exists(file.replace(".xtc", ".trr")): file = file.replace(".xtc", ".trr")
      
      cmd = "echo 'cleanedAtoms' | %s trjconv -f %s -s %s -o %s -pbc mol -n %s"%(gmx, file, tpr, "/tmp/mcmd_processing_%s_%s.tmp.xtc"%(projectName, i), tools.analFolder+"temp/index.temp.ndx")
      procs.append(subprocess.Popen(cmd, shell=True))
    for p in procs: p.wait()
      
    cleanFile(tools.analFolder+"cleaned/%s.cat.xtc"%seed)
    os.system("%s trjcat -f /tmp/mcmd_processing_%s_*.tmp.xtc -o %s"%(gmx, projectName, tools.analFolder+"cleaned/%s.cat.xtc"%seed))
    for i in range(len(info)): os.remove("/tmp/mcmd_processing_%s_%s.tmp.xtc"%(projectName, i))
    
    if tools.cli_args.fix_ligand:
      xtc_out = mdtraj.formats.XTCTrajectoryFile(tools.analFolder+"cleaned/%s.xtc"%seed, "w")
      for frame in mdtraj.iterload(tools.analFolder+"cleaned/%s.cat.xtc"%seed, 1, top=struc):
        if (frame.time[0]*.001).is_integer():
          sys.stdout.write("Ligand-protein PBC fixing time: %d ns \r"%(frame.time[0]*.001) )
          sys.stdout.flush()
    
        box = mdtraj.utils.ensure_type(frame.unitcell_vectors, dtype=np.float32, ndim=3, name='unitcell_vectors', shape=(1, 3, 3), warn_on_cast=False)
        orthogonal = np.allclose(frame.unitcell_angles, 90)
      
        r = mdtraj.compute_distances(frame, pairs)[0]
        nearest_pair = pairs[np.argmin(r)]
        xyzs[0] = frame.xyz[0,nearest_pair]
        mdtraj.geometry._geometry._dist_mic_displacement(xyzs, fake_pairs, box.transpose(0, 2, 1).copy(), displacement, orthogonal)
        frame.xyz[0,ligAtoms] += (xyzs[0,1]-xyzs[0,0]) + displacement[0][0]
        xtc_out.write(frame.xyz, frame.time, frame.time/dt, frame.unitcell_vectors)
      xtc_out.close()
    elif custom_pbc_fix is not None: 
      traj = custom_pbc_fix(tools, tools.analFolder+"cleaned/%s.cat.xtc"%seed)
      traj.save(tools.analFolder+"cleaned/%s.xtc"%seed)
      os.remove(tools.analFolder+"cleaned/%s.cat.xtc"%seed)
    else: os.rename(tools.analFolder+"cleaned/%s.cat.xtc"%seed, tools.analFolder+"cleaned/%s.xtc"%seed)

prepAux()
fixXTC()
