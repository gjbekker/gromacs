import numpy as np, mdtraj, os, yieldxtc, sys

import tools

def additionalArguments(optparser):
  optparser.add_argument("temp", type=int, default=300, nargs="?")
  optparser.add_argument("N", type=int, default=1000, nargs="?")
  optparser.add_argument("mcSeed", type=int, default=12345, nargs="?")

tools.setup(additionalArguments)

tools.initStructure()

Nbakapara = tools.config["fbmd"]["seeds"]

outputIndexes = tools.outputIndexes() # outIndexes receptorCalpha receptorAtoms ligandAtoms

struc = tools.baseStruct.atom_slice(outputIndexes.raw, inplace=False)

fileinfo = tools.findProdData()

if not os.path.exists(tools.analFolder+"cleaned"): os.mkdir(tools.analFolder+"cleaned")
if not os.path.exists(tools.analFolder+"ref.gro"): struc.save_gro(tools.analFolder+"ref.gro")

dt = tools.config["base"].get("dt", 2)*.001

temp = tools.cli_args.temp
N = tools.cli_args.N
mcSeed = tools.cli_args.mcSeed

if temp == -1: bins, pdf_log = tools.getReweigingProb(300)
else: bins, pdf_log = tools.getReweigingProb(temp)
pdf_min = bins[0]
nob = len(pdf_log)-1
pdf_binSize = bins[1]-bins[0]

lnP = []
eneList = []
fromSeeds = []
seedIndices = []

for seed in range(Nbakapara):  
  ene = np.fromfile(tools.analFolder+"cleaned/%s.fb.ene"%seed, dtype=np.float32)
  bins = np.floor((ene-pdf_min)/pdf_binSize).astype(int)
  bins[bins<0] = 0
  bins[bins>nob] = nob
  lnP.append(pdf_log[bins])
  eneList.append(ene)
  fromSeeds.append(np.full(len(ene), seed, dtype=int))
  seedIndices.append(np.arange(len(ene)))
  
lnP = np.concatenate(lnP)
eneList = np.concatenate(eneList)
fromSeeds = np.concatenate(fromSeeds)
seedIndices = np.concatenate(seedIndices)

P = np.exp(lnP)
P /= np.sum(P)
np.random.seed(mcSeed)
if temp == -1: structureIndices = np.random.choice(len(P), N, False)
else: structureIndices = np.random.choice(len(P), N, False, P)

outinfo = {}
outmeta = {}

for i in structureIndices:
  seed = fromSeeds[i]
  idx = seedIndices[i]
  
  if not seed in outinfo: 
    outinfo[seed] = []
    outmeta[seed] = {}
  outinfo[seed].append(idx)
  outmeta[seed][idx] = i

xtc_out = mdtraj.formats.XTCTrajectoryFile(tools.analFolder+"mc_%s_%s_%s.xtc"%(temp, N, mcSeed), "w")
txt_out = open(tools.analFolder+"mc_%s_%s_%s.txt"%(temp, N, mcSeed), "w")

tmp = tools.load(tools.analFolder+"ref.gro")
indicesSI = outputIndexes.receptorCalpha

for seed,idxs in outinfo.items(): 
  idxs = set(idxs)

  xtc_in = yieldxtc.XTCTrajectoryFile(tools.analFolder+"cleaned/%s.xtc"%seed)
  
  for i, (xyz, time, step, box) in enumerate(xtc_in.readyield()):
    if not i in idxs: continue
    
    tmp.xyz[0] = xyz
    tmp.superpose(struc, 0, indicesSI)
    
    xtc_out.write(tmp.xyz[0], time, seed, box)
    txt_out.write("%s %s %s %s\n"%(seed, time, lnP[outmeta[seed][i]], eneList[outmeta[seed][i]]))
    
    idxs.remove(i)
    if len(idxs) == 0: break # done
  xtc_in.close()

xtc_out.close()
txt_out.close()
