import os, argparse


parser = argparse.ArgumentParser(description='Generates comres files...')
parser.add_argument('-f', help="Gro file", dest="groFile")
parser.add_argument('-o', help="Out file", dest="outFile")
parser.add_argument('-p', help="Topology file", dest="tplFile")
parser.add_argument('-n', help="Index file", dest="indexFile")
parser.add_argument('-r', help="Receptor group", dest="receptorGroup", default="Protein")
parser.add_argument('--atoms', help="Atoms to restrain", dest="atoms", default="CA")

args = parser.parse_args()

groFile = args.groFile
outFile = args.outFile
tplFile = args.tplFile
indexFile = args.indexFile
atoms = set(args.atoms.split(","))

receptorGroup = args.receptorGroup

def loadTOP(loc, bin=None):
  if bin == None:
    bin = {}
    bin["__atoms"] = []
  path_alt = gmx.replace("build/bin/gmx", "share/top/")
  
  path = os.path.dirname(loc)+os.sep
  mode, MT = None, None
  for line in open(loc).readlines():
    ls = line.strip()
    if not ls: continue
    if ls[:8] == "#include":
      inc = line[9:].replace("\"", "").strip()
      if os.path.exists(path+inc): loadTOP(path+inc, bin)
      elif os.path.exists(path_alt+inc): loadTOP(path_alt+inc, bin)
    elif line[:1] != ";":
      if line[0] == "[":
        mode = ls.strip("[]").strip()
        continue
      if mode == "moleculetype":
        MT = line.split()[0]
        bin[MT] = {}
      if mode == "atoms" and ls[0] != "#":
        if not bin[MT].has_key("atoms"): bin[MT]["atoms"] = []
        bin[MT]["atoms"].append(ls.split())  
      if mode == "molecules":
        tmp = line.strip().split()
        for i in xrange(int(tmp[1])): bin["__atoms"] += bin[tmp[0]]["atoms"]
  return bin

__data__ = None
def loadData():
  global __data__

  if __data__ != None: return __data__
  
  __data__ = {}

  tpl_file = tplFile
  index_file = indexFile
  
  index, key = {}, None
  for line in open(index_file).readlines():
    line = line.strip()
    if line[:1] == "[":
      key = line.replace("[", "").replace("]", "").strip()
      index[key] = []
    elif key: index[key] += [int(i) for i in line.split()]
  
  __data__["index"] = index
  
  #__data__["topol"] = loadTOP(tpl_file)
  
  return __data__
  
loadData()

pdb = open(groFile).readlines()[2:-1]

coords = []
for a in __data__["index"][receptorGroup]:
  line = pdb[a-1]
  if not line[11:15].strip() in atoms: continue
  idx = int(line[15:20].strip())-1
  x = line[20:28].strip()
  y = line[28:36].strip()
  z = line[36:44].strip()
  coords.append("%s\t%s\t%s\t%s"%(idx, x, y, z))
 
open(outFile, "w").write("%s\n"%len(coords) + "\n".join(coords))
