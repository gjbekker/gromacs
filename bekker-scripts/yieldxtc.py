import mdtraj

class XTCTrajectoryFile(mdtraj.formats.XTCTrajectoryFile):
    def __init__(self, *args, **kwargs):
      super(XTCTrajectoryFile, self).__init__()

    def readyield(self, n_frames=None, stride=None, atom_indices=None):
        """read(n_frames=None, stride=None, atom_indices=None)
        Read data from an XTC file
        Parameters
        ----------
        n_frames : int, None
            The number of frames you would like to read from the file.
            If None, all of the remaining frames will be loaded.
        stride : int, optional
            Read only every stride-th frame.
        atom_indices : array_like, optional
            If not none, then read only a subset of the atoms coordinates from the
            file. This may be slightly slower than the standard read because it required
            an extra copy, but will save memory.
        Returns
        -------
        xyz : np.ndarray, shape=(n_frames, n_atoms, 3), dtype=np.float32
            The cartesian coordinates, in nanometers
        time : np.ndarray, shape=(n_frames), dtype=np.float32
            The simulation time, in picoseconds, corresponding to each frame
        step : np.ndarray, shape=(n_frames), dtype=np.int32
            The step in the simulation corresponding to each frame
        box : np.ndarray, shape=(n_frames, 3, 3), dtype=np.float32
            The box vectors in each frame.
        See Also
        --------
        read_as_traj : Returns a Trajectory object
        """
        
        # TODO: use offsets if stride is set
        
        idx = 0

        while True:
            xyz, time, step, box, status = self._read(1, atom_indices, 1)
            if len(xyz) <= 0 or (n_frames != None and idx > n_frames): break
            if stride and idx%stride != 0: continue

            idx += 1
            yield (xyz[0], time[0], step[0], box[0])
