import sys, os, yieldxtc, mdtraj

cwd = os.getcwd()+os.sep
import tools

def additionalArguments(optparser):
  optparser.add_argument("snapshots", type=str, nargs="?")

tools.setup(additionalArguments)

mdx = tools.RESTART["production-phase"]

xtcLoc = tools.analFolder+"../mdx-%(mdx)s/%(seed)s.xtc"

info = {}

ref = tools.load(tools.analFolder+"ref.gro")
struc = tools.load(tools.analFolder+"ref.gro")

outputIndexes = tools.outputIndexes() # outIndexes receptorCalpha receptorAtoms ligandAtoms
indicesSI = outputIndexes.receptorCalpha
if tools.config["analysis"].get("si-posres"):
  prfile = tools.config["fbmd"].get("restraints", {}).get("posres-file")
  if not prfile: 
    print("No position restraints used, while si-posres was set...")
    exit()
  indicesSI = []
  for line in open(tools.CWD+prfile).readlines()[1:]:
    if not line.strip(): continue
    line = line.split()
    indicesSI.append(np.where(int(line[0]) == outputIndexes.raw)[0][0])
  indicesSI = np.array(indicesSI)
  print(len(indicesSI), "atoms selected for superposition")


for i in tools.cli_args.snapshots:
  if ":" in i:
    seed, sidx = i.split(":")
    if not seed in info: info[seed] = {}
    info[seed][int(sidx)] = "%s_%s.gro"%(seed, sidx)

for seed, v in info.items():
  fp = yieldxtc.XTCTrajectoryFile(tools.analFolder+"cleaned/%s.xtc"%seed)
  
  tooutput = sorted(v.keys())
  next = tooutput.pop(0)
  for i, (xyz, time, step, box) in enumerate(fp.readyield()):
    if i != next: continue
    
    struc.xyz[0] = xyz
    struc.superpose(ref, 0, indicesSI)
    struc.save(v[next].replace(".gro", ".pdb"))
    
    if len(tooutput) == 0: break # done
    next = tooutput.pop(0)
  fp.close()
