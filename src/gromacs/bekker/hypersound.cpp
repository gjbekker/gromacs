
#include "bekker.h"

#include <math.h>

#include "gromacs/mdtypes/commrec.h"
#include "gromacs/domdec/collect.h"

void araki_apply_hypersound(t_hypersound_bag *hypersound_bag, rvec *x, rvec *v, const matrix box, int64_t step, const t_commrec *cr) {
  int cyc_num2 = step%hypersound_bag->N;
  if (cyc_num2 != 0) return;
  
  int cyc_num = step/hypersound_bag->N;
  
	int j=cyc_num/hypersound_bag->shock_steps; //corresponding to the irradiation time of each shock wave (80N)
	int k=cyc_num/(hypersound_bag->shock_steps*6); //corresponding to the irradiation time of a set of shock waves in the six directions (480N)
	int l=j-6*k; //switching of shock waves (0->+X, 1->+Y, 2->+Z, 3->-X, 4->-Y, 5->-Z)
  real max_vel = hypersound_bag->max_vel;
  real border = hypersound_bag->border;
  real coef=cos(cyc_num/16.0*2*M_PI);
  
  int d = 0;
  bool plusMode = l == 0 || l == 2 || l == 4;
  if (l == 0 || l == 1) d = 0; // x
  else if (l == 2 || l == 3) d = 1; // y
  else if (l == 4 || l == 5) d = 2; // z
  
  if (k%6 == 0) {
    auto localAtomIndices = hypersound_bag->solvent.atomSet->localIndex();
    for (size_t i=0; i<localAtomIndices.size(); i++) {
      if (plusMode && x[i][d] >= 0 && x[i][d] <= border) v[i][d] += max_vel*coef; //####### sets the maximum velocity (vmax) in the +X/Y/Z direction. (e.g. 0.4 km/s)  ########
      else if (! plusMode && x[i][d] >= box[d][d]-border && x[i][d] <= box[d][d]) v[i][d] -= max_vel*coef; //####### sets the maximum velocity (vmax) in the -X/Y/Z direction. (e.g. 0.4 km/s)  ########
    }
  }
}
