
#include "bekker.h"


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <stdlib.h>

#include "gromacs/mdtypes/commrec.h"
#include "gromacs/gmxlib/network.h"

t_bekker_data bekker_config = t_bekker_data();

void put_point_in_box(PbcType ePBC, const matrix box, rvec x) {
  int npbcdim, m, d;

  if (ePBC == PbcType::XY) npbcdim = 2;
  else npbcdim = 3;

  if (TRICLINIC(box)) {
    for (m = npbcdim-1; m >= 0; m--) {
      while (x[m] < 0) for (d = 0; d <= m; d++) x[d] += box[m][d];
      while (x[m] >= box[m][m]) for (d = 0; d <= m; d++) x[d] -= box[m][d];
    }
  }
  else {
    for (d = 0; d < npbcdim; d++) {
      while (x[d] < 0) x[d] += box[d][d];
      while (x[d] >= box[d][d]) x[d] -= box[d][d];
    }
  }
}

void bekker_calc_initial_com(t_bekker_group *bgrp, rvec *x, PbcType ePBC, const matrix box, rvec refPoint) { // global only
  // calculate the com in a pbc safe manner...
  int ii, d;
  real wmass;
  double inv_wm = bgrp->invtm;
  rvec dx; rvec com;
  for (d=0; d<DIM; d++) com[d] = 0;
  
  // ePBC, box
  
  t_pbc  pbc;
  set_pbc(&pbc, ePBC, box);

  rvec rp;
  if (refPoint == NULL) for (d=0; d<DIM; d++) rp[d] = x[bgrp->global_atoms[0]][d];
  else for (d=0; d<DIM; d++) rp[d] = refPoint[d];
  for (size_t i=0; i<bgrp->global_atoms.size(); i++) {
    ii = bgrp->global_atoms[i];
    wmass = bgrp->masses[i] * inv_wm;
    
    pbc_dx(&pbc, x[ii], rp, dx);
    for (d=0; d<DIM; d++) com[d] += (rp[d]+dx[d])*wmass;
    
    // calculate COM while making the molecule whole, using the previous atom as the reference (this should enable even larger molecules? -> however there might be issues with multi-molecule groups...)
    if (refPoint == NULL) for (d=0; d<DIM; d++) rp[d] += dx[d];
  }

  for (d=0; d<DIM; d++) bgrp->com[d] = com[d];

  // now, put the COM back inside the box
  if (refPoint == NULL) put_point_in_box(ePBC, box, bgrp->com);
}

void bekker_calc_com(t_bekker_group *bgrp, rvec *x, PbcType ePBC, const matrix box, const t_commrec *cr) {
  int ii, d, gidx;
  real wmass;
  rvec dxyz, dx;
  clear_rvec(dxyz);
  double inv_wm = bgrp->invtm;
  auto localAtomIndices = bgrp->atomSet->localIndex();
  auto search = bgrp->atomSet->collectiveIndex();
  
  t_pbc  pbc;
  set_pbc(&pbc, ePBC, box);
  
  for (size_t i=0; i<localAtomIndices.size(); i++) {
    ii = localAtomIndices[i];
    gidx = search[i];
    wmass = bgrp->masses[gidx] * inv_wm;
    
    pbc_dx(&pbc, x[ii], bgrp->groupAtomLocs[gidx], dx);
    for (d=0; d<DIM; d++) dxyz[d] += dx[d] * wmass;
  }

  if (PAR(cr)) gmx_sum(3, dxyz, cr);
  bgrp->com[0] += dxyz[0];
  bgrp->com[1] += dxyz[1];
  bgrp->com[2] += dxyz[2];
}

void project2Path(const t_path_bag *path_bag, const rvec com, rvec *lambda_3d, real *lambda, rvec *dir, int *idx_out) {
  real r_nearest = 100000000.0, tmp, ratio, uv, ddp;
  rvec dxyz, temp;
  int idx, i, i_nearest = 0;
  gmx_bool extrapolate = 0;

  // find the nearest waypoint
  for (i=0; i<path_bag->nPath+1; i++) {
    rvec_sub(path_bag->path[i], com, dxyz);
    tmp = iprod(dxyz, dxyz);
    if (tmp < r_nearest) {
      i_nearest = i;
      r_nearest = tmp;
    }
  }

  if (i_nearest == 0) {
    idx = 0;
    rvec_sub(com, path_bag->path[idx], dxyz);
    if (iprod(path_bag->tangents[0], dxyz) < 0) extrapolate = 1;
  }
  else if (i_nearest >= path_bag->nPath) {
    idx = path_bag->nPath;
    rvec_sub(com, path_bag->path[idx], dxyz);
    if (iprod(path_bag->tangents[idx], dxyz) > 0) extrapolate = 1;
    else {
      idx -= 1;
      rvec_sub(com, path_bag->path[idx], dxyz);
    }
  }
  else {
    rvec_sub(com, path_bag->path[i_nearest], dxyz);
    if (iprod(path_bag->tangents[i_nearest], dxyz) < 0) {
      idx = i_nearest-1;
      rvec_sub(com, path_bag->path[idx], dxyz);
    }
    else idx = i_nearest;
  }

  (*dir)[0] = path_bag->pathDirections[idx][0];
  (*dir)[1] = path_bag->pathDirections[idx][1];
  (*dir)[2] = path_bag->pathDirections[idx][2];

  *idx_out = idx;

  if (extrapolate || path_bag->linear) {
    ratio = iprod(*dir, dxyz); // magnitude
    *lambda = path_bag->waypoints_lambda[idx] + ratio; // 1D lambda

    svmul(ratio, path_bag->pathDirections[idx], *lambda_3d);
    rvec_add(*lambda_3d, path_bag->path[idx], *lambda_3d);
  }
  else {
    rvec_sub(com, path_bag->centerPoints[idx], dxyz);

    svmul(iprod(path_bag->planeVectors[idx], dxyz), path_bag->planeVectors[idx], temp);
    rvec_sub(dxyz, temp, dxyz);
    svmul(gmx::invsqrt(norm2(dxyz)), dxyz, dxyz);

    uv = iprod(path_bag->refVectors[idx], dxyz);
    if (uv < 0) uv = -uv;
    ddp = 1 - (uv+1)*.5;

    ratio = ddp/path_bag->maxAngles[idx];

    // calculate lambda
    *lambda = path_bag->waypoints_lambda[idx] + (ratio*path_bag->pathVectorsMagnitude[idx]);

    // calculate lambda3d
    svmul(ratio*path_bag->pathVectorsMagnitude[idx], path_bag->pathDirections[idx], *lambda_3d);
    rvec_add(*lambda_3d, path_bag->path[idx], *lambda_3d);

    (*dir)[0] = path_bag->tangents[idx][0] + ratio * (path_bag->tangents[idx+1][0] - path_bag->tangents[idx][0]);
    (*dir)[1] = path_bag->tangents[idx][1] + ratio * (path_bag->tangents[idx+1][1] - path_bag->tangents[idx][1]);
    (*dir)[2] = path_bag->tangents[idx][2] + ratio * (path_bag->tangents[idx+1][2] - path_bag->tangents[idx][2]);
    svmul(gmx::invsqrt(norm2(*dir)), *dir, *dir);
  }

}

void dd_register_group(t_bekker_group *ligand, gmx::LocalAtomSetManager* atomSets) {
  ligand->atomSet_baka.push_back(atomSets->add(ligand->global_atoms));
  ligand->atomSet = &ligand->atomSet_baka[0];
}

